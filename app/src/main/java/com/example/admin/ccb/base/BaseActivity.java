package com.example.admin.ccb.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.ccb.R;
import com.example.admin.ccb.activity.LoginActivity;
import com.example.admin.ccb.config.Config;
import com.example.admin.ccb.utils.DialogUtils;
import com.example.admin.ccb.utils.SPUtils;
import com.google.gson.Gson;
import com.gyf.barlibrary.ImmersionBar;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.HttpParams;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import www.ccb.com.common.utils.LogUtils;
import www.ccb.com.common.utils.ToastUtils;

public abstract class BaseActivity extends AppCompatActivity {

    public Context mContext;
    private Gson gson;
    public Bundle savedInstanceState;
    public ImmersionBar mImmersionBar;
    private Unbinder unbind;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentViewResource());
        this.savedInstanceState = savedInstanceState;
        unbind = ButterKnife.bind(this);
        mImmersionBar = ImmersionBar.with(this);
        mImmersionBar.init();  //所有子类都将继承这些相同的属性
        mContext = this;
        gson = new Gson();
        initView();
        initData();
        initList();
        titleView();
    }

    public abstract int getContentViewResource();
    protected abstract void initView();
    protected abstract void initData();
    protected abstract void initList();
    @SuppressLint("ResourceType")
    public void UpTitle(String title){
        if (findViewById(R.id.vBar) == null)return;
        mImmersionBar.titleBar(R.id.vBar).statusBarDarkFont(true, 0.2f).init();
        findViewById(R.id.tvTitleBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ((TextView)findViewById(R.id.tvTitleBar)).setText(title == null?"":title);
    }
    public void titleView(){
        View view = View.inflate(mContext,R.layout.layout_title_view,null);
        TextView titleTextView = view.findViewById(R.id.title_view_title);
        TextView rightTextView = view.findViewById(R.id.right_text);
        ImageView rightImgView = view.findViewById(R.id.right_image);
        ImageView leftImgView = view.findViewById(R.id.left_image);
        leftImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseActivity.this.finish();
            }
        });
    }
    public void okGetRequest(final String url){
        OkGo.<String>get(url).execute(new StringCallback() {
            @Override
            public void onStart(com.lzy.okgo.request.base.Request<String, ? extends com.lzy.okgo.request.base.Request> request) {
                super.onStart(request);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<String> response) {
                    okResponseSuccess(url, response.body());
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<String> response) {
                super.onError(response);
                okResponseError(url, response.body());
            }

            @Override
            public void onFinish() {
                super.onFinish();
                okResponseFinish(url);
            }
        });
    }

    /**
     * OK网络请求 不需要传mobileLogin和JSESSIONID
     *
     * @param httpurl      请求URl 也用来标记
     * @param params       请求参数
     * @param clazz        返回的Bean对象
     * @param DialogMsg    弹出Dialog的文字消息
     * @param isShowDialog 是否弹出Dialog 默认弹出
     */
    public void okPostRequest(final String what, final String httpurl, HttpParams params, final Class clazz, final String DialogMsg, final boolean isShowDialog) {
        final String url = httpurl + "";
        params.put("mobileLogin", true);
        OkGo.<String>post(url).params(params).execute(new StringCallback() {
            @Override
            public void onStart(com.lzy.okgo.request.base.Request<String, ? extends com.lzy.okgo.request.base.Request> request) {
                super.onStart(request);
                if (isShowDialog) showCbDialog(DialogMsg);
                okResponseStart(httpurl + what);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<String> response) {
                LogUtils.out(httpurl + what + "请求结果:__", response.body());
                if (clazz == null) {
                    okResponseSuccess(what, response.body());
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if(!jsonObject.get("code").equals("success")){
                            String msg = jsonObject.getString("message");
                            okResponseError(what,msg);
                            return;
                        }
                        Object bean = gson.fromJson(jsonObject.get("data").toString(), clazz);
                        if(null == bean){
                            okResponseError(what,"网络出错");
                        }
                        okResponseSuccess( what, bean);
                    } catch (Exception e) {
                        okResponseError(what,"网络出错");
                        ToastUtils.GsonExtremely();
                        LogUtils.e("异常信息：" + e.toString());
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<String> response) {
                super.onError(response);
                LogUtils.out(httpurl + what + "请求结果:__", response.body());
                okResponseError(httpurl + what, response.body());
                ToastUtils.failNetRequest();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (isShowDialog) dismissCbDialog();
                okResponseFinish(httpurl + what);
            }
        });
    }
    /**
     * OK网络请求 不需要传mobileLogin和JSESSIONID
     *
     * 判断是否登录  不登录跳转登录
     *
     * @param httpurl      请求URl 也用来标记
     * @param params       请求参数
     * @param clazz        返回的Bean对象
     * @param DialogMsg    弹出Dialog的文字消息
     * @param isShowDialog 是否弹出Dialog 默认弹出
     */
    public void okPostRequestLogin(final String what, final String httpurl, HttpParams params, final Class clazz, final String DialogMsg, final boolean isShowDialog) {
        final String url = httpurl + "";
        params = isLogin(params);
        if(params == null)
            return;
        LogUtils.out(httpurl + what + "请求参数:__", params.toString());
        OkGo.<String>post(url).params(params).execute(new StringCallback() {
            @Override
            public void onStart(com.lzy.okgo.request.base.Request<String, ? extends com.lzy.okgo.request.base.Request> request) {
                super.onStart(request);
//                if (isShowDialog) showCbDialog(DialogMsg);
                okResponseStart( what);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<String> response) {
                LogUtils.out(httpurl + what + "请求结果:__", response.body());
                if (clazz == null) {
                    okResponseSuccess(what, response.body());
                } else {


                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if(!jsonObject.get("code").equals("success")){
                            String msg = jsonObject.getString("message");
                            okResponseError(what,msg);
                            return;
                        }
                        Object bean = gson.fromJson(jsonObject.get("data").toString(), clazz);
                        if(null == bean){
                            okResponseError(what,"服务器开小差了");
                            return;
                        }
                        okResponseSuccess( what, bean);
                    } catch (Exception e) {
                        okResponseError(what,"网络出错");
                        ToastUtils.GsonExtremely();
                        LogUtils.e("异常信息：" + e.toString());
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<String> response) {
                super.onError(response);
                LogUtils.out( what + "请求结果:__", response.body());
                okResponseError( what, response.body());
                ToastUtils.failNetRequest();
            }

            @Override
            public void onFinish() {
                super.onFinish();
//                if (isShowDialog) dismissCbDialog();
                okResponseFinish( what);
            }
        });
    }

    private HttpParams isLogin(HttpParams params) {
        if(null == SPUtils.get(mContext, Config.TOKEN,String.class)){
            LoginActivity.create(mContext);
            return null;
        }else{
            params.put("tokenId",SPUtils.get(mContext, Config.TOKEN,String.class).toString());
            return params;
        }
    }

    private void dismissCbDialog() {
        DialogUtils.dissmissProgressDialog();
    }

    private void showCbDialog(String dialogMsg) {
        DialogUtils.showProgressDialogForIosStyle(mContext,dialogMsg,false);

    }

    /**
     * OK网络请求成功回调
     *
     * @param whit 请求标记
     * @param t    返回结果
     */
    protected void okResponseSuccess(String whit, Object t) {
    }

    /**
     * OK网络请求失败（错误）回调
     *
     * @param whit
     * @param body
     */
    protected void okResponseError(String whit, String body) {
    }

    /**
     * OK网络开始请求回调
     * @param flag
     */
    protected void okResponseStart(String flag) {
    }

    /**
     * OK网络请求完成回调
     * @param flag
     */
    protected void okResponseFinish(String flag) {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbind.unbind();
        if (mImmersionBar != null)
            mImmersionBar.destroy();  //必须调用该方法，防止内存泄漏，不调用该方法，如果界面bar发生改变，在不关闭app的情况下，退出此界面再进入将记忆最后一次bar改变的状态
    }
}
