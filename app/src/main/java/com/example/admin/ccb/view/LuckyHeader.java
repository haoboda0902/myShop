package com.example.admin.ccb.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.admin.ccb.R;

/**
 * 创建日期：2019/3/23 on 15:55
 * 描述:
 */

public class LuckyHeader extends FrameLayout {

    private TextView mPeriod;//期数
    private TextView mDigit;//位数
    private EditText mNumber;//购买号码
    private EditText mMltiple;//倍数
    private View mBuy;//购买
    private TextView mContent;//共需 ===

    public LuckyHeader(@NonNull Context context) {
        this(context, null);
    }

    public LuckyHeader(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LuckyHeader(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.lucky_head, this);
        mPeriod = findViewById(R.id.period);
        mDigit = findViewById(R.id.digit);
        mNumber = findViewById(R.id.number);
        mMltiple = findViewById(R.id.multiple);
        mBuy = findViewById(R.id.buy);
        mContent = findViewById(R.id.content);
    }


    public void bindDatas(Object o) {

    }


}
