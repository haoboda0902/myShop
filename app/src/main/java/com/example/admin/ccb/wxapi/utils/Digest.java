package com.example.admin.ccb.wxapi.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @description: 常用的加密算法
 * @author: tangmin
 * @date: 2019-01-30 10:47:55
 * @version: 1.0
 */
public class Digest {

  public static final String ENCODE = "UTF-8";

  public static String digest(String aValue) {
    return digest(aValue, ENCODE);

  }

  public static String digest(String aValue, String encoding) {
    aValue = aValue.trim();
    byte value[];
    try {
      value = aValue.getBytes(encoding);
    } catch (UnsupportedEncodingException e) {
      value = aValue.getBytes();
    }
    MessageDigest md = null;
    try {
      md = MessageDigest.getInstance("SHA");
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
      return null;
    }
    return ConvertUtils.toHex(md.digest(value));
  }

}
