package com.example.admin.ccb.fragment;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;

import com.example.admin.ccb.R;
import com.example.admin.ccb.adapter.IntegralAdapter;
import com.example.admin.ccb.view.CouponHeader;

import java.util.Arrays;

/**
 * 描述: 槟购券
 */

public class CouponFragment extends RecyclerFragment {
    private IntegralAdapter mIntegralAdapter;
    private CouponHeader couponHeader;

    public static CouponFragment newInstance(Bundle bundle) {
        CouponFragment couponFragment = new CouponFragment();
        if (bundle != null) {
            couponFragment.setArguments(bundle);
        }
        return couponFragment;
    }

    @Override
    public RecyclerView.Adapter getAdapter() {
        mIntegralAdapter = new IntegralAdapter();
        couponHeader = new CouponHeader(getActivity());
        mIntegralAdapter.addHeaderView(couponHeader);
        return mIntegralAdapter;
    }

    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider));
        return dividerItemDecoration;
    }

    @Override
    public void loadData() {
        mIntegralAdapter.addData(Arrays.asList(new Object[]{
                new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object()
        }));
        couponHeader.bindDatas(null);
    }

    @Override
    public void initListener() {

    }
}
