package com.example.admin.ccb.activity;


import android.support.v4.app.Fragment;

import com.example.admin.ccb.R;
import com.example.admin.ccb.fragment.CouponFragment;
import com.example.admin.ccb.fragment.IntegralFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 描述: 积分柜
 */

public class IntegralCabinetActivity extends TabLayoutViewpagerBaseActivity {
    private List<Fragment> list = new ArrayList<>();

    @Override
    protected void initView() {
        super.initView();
        setHeaderTitle("积分柜");
    }

    @Override
    public String[] getTitles() {
        return getResources().getStringArray(R.array.integral_cabinet_titles);
    }

    @Override
    public ArrayList<Fragment> getFragments() {
        list.add(CouponFragment.newInstance(null));
        list.add(IntegralFragment.newInstance(null));
        return (ArrayList<Fragment>) list;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initList() {

    }
}
