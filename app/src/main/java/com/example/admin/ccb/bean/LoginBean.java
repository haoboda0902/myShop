package com.example.admin.ccb.bean;

public class LoginBean  {


    /**
     * shareCode : B62LG7
     * tokenId : qbpdlWmyPk1fq5nwWU20190318225656
     * mobile : 18801134486
     * id : 11
     * state : 0
     * userName : 18801134486
     * cardNumber : VIP-2019031723375348C577TW
     */

    public String shareCode;
    public String tokenId;
    public String mobile;
    public int id;
    public String state;
    public String userName;
    public String cardNumber;

    public String getShareCode() {
        return shareCode;
    }

    public void setShareCode(String shareCode) {
        this.shareCode = shareCode;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
