package com.example.admin.ccb.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;

import com.example.admin.ccb.R;
import com.example.admin.ccb.adapter.TransferRecordAdapter;
import com.example.admin.ccb.view.SpacesItemDecoration;

import java.util.Arrays;

/**
 * 创建日期：2019/3/23 on 10:07
 * 描述:转让记录
 */

public class TransferRecordActivity extends RecyclerViewBaseActivity {
    private TransferRecordAdapter mTransferRecordAdapter;

    public static void open(Context context) {
        Intent intent = new Intent(context, TransferRecordActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void initData() {
        mTransferRecordAdapter.addData(Arrays.asList(new Object[]{
                new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object()
        }));
    }

    @Override
    protected void initList() {

    }

    @Override
    public RecyclerView.Adapter getAdapter() {
        mTransferRecordAdapter = new TransferRecordAdapter();
        return mTransferRecordAdapter;
    }

    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        return new SpacesItemDecoration((int) getResources().getDimension(R.dimen.dp10));
    }

    @Override
    public String getTitleString() {
        return "转让记录";
    }

}
