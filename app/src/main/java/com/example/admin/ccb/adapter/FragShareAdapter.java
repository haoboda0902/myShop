package com.example.admin.ccb.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.admin.ccb.bean.HomeShareList;

import java.util.List;

public class FragShareAdapter extends BaseQuickAdapter<HomeShareList,BaseViewHolder> {

    public FragShareAdapter(int layoutResId, @Nullable List<HomeShareList> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, HomeShareList item) {

    }
}
