package com.example.admin.ccb.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.admin.ccb.R;
import com.example.admin.ccb.utils.SpanableUtils;

/**
 * 创建日期：2019/3/23 on 15:36
 * 描述:
 */

public class IntegralAdapter  extends BaseQuickAdapter<Object, BaseViewHolder> {

    public IntegralAdapter() {
        super(R.layout.item_integral);
    }

    @Override
    protected void convert(BaseViewHolder helper, Object item) {
        int postion = helper.getLayoutPosition();
        int color_green = helper.getConvertView().getResources().getColor(R.color.color_60b11c);
        int color_red = helper.getConvertView().getResources().getColor(R.color.color_fd3535);
        helper.setText(R.id.desc, "预约美容养颜？？？？？");
        helper.setText(R.id.date, "2018-02-06 12:33:44");

        if (postion % 2 == 0) {
            helper.setText(R.id.number, "+88");
            helper.setTextColor(R.id.number, color_green);
        } else {
            helper.setText(R.id.number, "-99");
            helper.setTextColor(R.id.number, color_red);
        }
    }
}
