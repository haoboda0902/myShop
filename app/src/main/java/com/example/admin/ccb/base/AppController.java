package com.example.admin.ccb.base;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;


import com.example.admin.ccb.utils.PermissionUtils;

import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class AppController {
    private static AppController instance;
    private Stack<Activity> activityHandle = new Stack();
    static WeakReference<Activity> sTopActivityWeakRef;
    static List<Activity> sActivityList = new LinkedList();
    private static Application.ActivityLifecycleCallbacks mCallbacks = new Application.ActivityLifecycleCallbacks() {
        public void onActivityCreated(Activity activity, Bundle bundle) {
            AppController.sActivityList.add(activity);
            AppController.setTopActivityWeakRef(activity);
        }

        public void onActivityStarted(Activity activity) {
            AppController.setTopActivityWeakRef(activity);
        }

        public void onActivityResumed(Activity activity) {
            AppController.setTopActivityWeakRef(activity);
        }

        public void onActivityPaused(Activity activity) {
        }

        public void onActivityStopped(Activity activity) {
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityDestroyed(Activity activity) {
            AppController.sActivityList.remove(activity);
        }
    };

    private AppController() {
    }

    public static AppController initAppController(Application application) {
        application.registerActivityLifecycleCallbacks(mCallbacks);

        if (instance == null) {
            instance = new AppController();
        }

        return instance;
    }

    public void push(Activity activity) {
        this.activityHandle.push(activity);
    }

    public void remove(Activity activity) {
        this.activityHandle.remove(activity);
    }

    public Activity getOnce() {
        return (Activity) this.activityHandle.lastElement();
    }

    public Activity findActivity(String var1) {
        Iterator iterator = this.activityHandle.iterator();

        Activity activity;
        do {
            if (!iterator.hasNext()) {
                return null;
            }

            activity = (Activity) iterator.next();
        } while (!activity.getClass().getSimpleName().equals(var1));

        return activity;
    }

    public void finishAll() {
        Iterator iterator = this.activityHandle.iterator();

        while (iterator.hasNext()) {
            Activity activity = (Activity) iterator.next();
            activity.finish();
        }

        this.activityHandle.removeAllElements();
    }

    private static void setTopActivityWeakRef(Activity activity) {
        if (activity.getClass() != PermissionUtils.PermissionActivity.class) {
            if (sTopActivityWeakRef == null || !activity.equals(sTopActivityWeakRef.get())) {
                sTopActivityWeakRef = new WeakReference(activity);
            }

        }
    }

    public static Activity getTopActivity() {
        if (sTopActivityWeakRef != null) {
            Activity activity = (Activity) sTopActivityWeakRef.get();
            if (activity != null) {
                return activity;
            }
        }

        List activityList = sActivityList;
        int var1 = activityList.size();
        return var1 > 0 ? (Activity) activityList.get(var1 - 1) : null;
    }
}
