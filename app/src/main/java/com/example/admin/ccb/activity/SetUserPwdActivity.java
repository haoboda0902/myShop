package com.example.admin.ccb.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.admin.ccb.MainActivity;
import com.example.admin.ccb.MyApplication;
import com.example.admin.ccb.R;
import com.example.admin.ccb.base.AppController;
import com.example.admin.ccb.base.BaseActivity;
import com.example.admin.ccb.utils.AppUtils;

import butterknife.BindView;
import butterknife.OnClick;
import www.ccb.com.common.utils.LogUtils;

public class SetUserPwdActivity extends BaseActivity {

    @BindView(R.id.register_pwd_card)
    TextView register_pwd_card;
    @BindView(R.id.register_commit)
    Button register_commit;
    @BindView(R.id.register_commit_skp)
    Button register_commit_skp;

    private String vipCard;


    public static Intent create(Context context, String vipCard, String token){
        Intent intent = new Intent(context,SetUserPwdActivity.class);
        intent.putExtra("vipCard",vipCard);
        intent.putExtra("token",token);
        return intent;
    }

    @Override
    public int getContentViewResource() {
        return R.layout.activity_setuserpwd;
    }

    @Override
    protected void initView() {

        vipCard = getIntent().getStringExtra("vipCard");
        register_pwd_card.setText(vipCard);
    }

    @OnClick({R.id.register_commit,R.id.register_commit_skp})
    public void OnClick(View view){
        switch (view.getId()){
            case R.id.register_commit:
                if(MyApplication.findActivity(new MainActivity(),"MainActivity")) {
                    this.finish();
                }
                else {
                    startActivity(MainActivity.create(mContext));
                    this.finish();
                }
                break;
            case R.id.register_commit_skp:
                    startActivity(RegisterHealthyActivity.create(mContext,vipCard,null));
                    this.finish();
                break;
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initList() {

    }
}
