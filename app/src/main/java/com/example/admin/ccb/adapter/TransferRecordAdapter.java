package com.example.admin.ccb.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.admin.ccb.R;
import com.example.admin.ccb.utils.SpanableUtils;

/**
 * 创建日期：2019/3/23 on 12:17
 * 描述:
 * 作者:wangfan
 */

public class TransferRecordAdapter extends BaseQuickAdapter<Object, BaseViewHolder> {

    public TransferRecordAdapter() {
        super(R.layout.item_transfer_record);
    }

    @Override
    protected void convert(BaseViewHolder helper, Object item) {
        int postion = helper.getLayoutPosition();
        int color_green = helper.getConvertView().getResources().getColor(R.color.color_60b11c);
        int color_red = helper.getConvertView().getResources().getColor(R.color.color_fd3535);
        helper.setText(R.id.date, "2018-8-18");
        if (postion % 2 == 0) {
            helper.setText(R.id.info, SpanableUtils.makeSpannableString(
                    helper.getConvertView().getResources().getString(R.string.record_in,
                            "张国荣 UXUDISAOJODSA").toString(), color_green));
            helper.setText(R.id.number, "+88");
            helper.setTextColor(R.id.number, color_green);
        } else {
            helper.setText(R.id.info,
                    SpanableUtils.makeSpannableString(helper.getConvertView().
                            getResources().getString(R.string.record_out,
                            "梅艳芳 daskjldsal"), color_red));
            helper.setText(R.id.number, "-99");
            helper.setTextColor(R.id.number, color_red);
        }
    }
}
