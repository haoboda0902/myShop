package com.example.admin.ccb.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class CommonFragmentPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> mDataList;
    private List<String> mTitleList;

    public CommonFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public int getCount() {
        return mDataList == null ? 0 : mDataList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return mTitleList == null ? super.getPageTitle(position) : mTitleList.get(position);
    }

    public void setTitleData(List<String> titleData) {
        mTitleList = titleData;
    }
    public List<String> getTitleList(){
        return mTitleList;
    }

    public void setData(List<Fragment> data) {
        this.mDataList = data;
        notifyDataSetChanged();
    }
}
