package com.example.admin.ccb.adapter;

import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.admin.ccb.R;

/**
 * 创建日期：2019/3/23 on 15:36
 * 描述:
 */

public class LuckyAdapter extends BaseQuickAdapter<Object, BaseViewHolder> {

    public LuckyAdapter() {
        super(R.layout.item_lucky);
    }

    @Override
    protected void convert(BaseViewHolder helper, Object item) {
       helper.setText(R.id.date,"20180632期");
       helper.setText(R.id.nubmer,"23");
       helper.getView(R.id.see).setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               /**
                * 查看中将
                */
           }
       });
    }
}
