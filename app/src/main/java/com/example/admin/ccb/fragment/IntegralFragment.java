package com.example.admin.ccb.fragment;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;

import com.example.admin.ccb.R;
import com.example.admin.ccb.adapter.IntegralAdapter;
import com.example.admin.ccb.view.CouponHeader;
import com.example.admin.ccb.view.IntegralHeader;

import java.util.Arrays;

/**
 * 描述:积分
 */

public class IntegralFragment extends RecyclerFragment {
    private IntegralAdapter mIntegralAdapter;
    private IntegralHeader integralHeader;

    public static IntegralFragment newInstance(Bundle bundle) {
        IntegralFragment integralFragment = new IntegralFragment();
        if (bundle != null) {
            integralFragment.setArguments(bundle);
        }
        return integralFragment;
    }

    @Override
    public RecyclerView.Adapter getAdapter() {
        mIntegralAdapter = new IntegralAdapter();
        integralHeader = new IntegralHeader(getActivity());
        mIntegralAdapter.addHeaderView(integralHeader);
        return mIntegralAdapter;
    }

    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(),DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider));
        return dividerItemDecoration;
    }

    @Override
    public void loadData() {
        mIntegralAdapter.addData(Arrays.asList(new Object[]{
                new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object()
        }));
        integralHeader.bindDatas(null);
    }

    @Override
    public void initListener() {

    }
}
