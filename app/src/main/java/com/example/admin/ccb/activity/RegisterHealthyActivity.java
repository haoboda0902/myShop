package com.example.admin.ccb.activity;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.example.admin.ccb.R;
import com.example.admin.ccb.base.BaseActivity;
import butterknife.BindView;
import butterknife.OnClick;
import www.ccb.com.common.utils.ToastUtils;

public class RegisterHealthyActivity extends BaseActivity {

    @BindView(R.id.register_healthy_blood)
    EditText register_healthy_blood;
    @BindView(R.id.register_healthy_weight)
    EditText register_healthy_weight;
    @BindView(R.id.register_healthy_height)
    EditText register_healthy_height;
    @BindView(R.id.register_healthy_history)
    EditText register_healthy_history;
    @BindView(R.id.register_healthy_commit)
    Button register_healthy_commit;

    public static Intent create(Context context,String vipCard,String token){
        Intent intent = new Intent(context,RegisterHealthyActivity.class);
        intent.putExtra("vipCard",vipCard);
        intent.putExtra("token",token);
        return intent;
    }

    @Override
    public int getContentViewResource() {
        return R.layout.activity_register_healthy;
    }

    @Override
    protected void initView() {

    }

    @OnClick({R.id.tvTitleBack,R.id.register_healthy_commit})
    public void OnClick(View view){
        switch (view.getId()){
            case R.id.tvTitleBack:
                RegisterHealthyActivity.this.finish();
                break;
            case R.id.register_healthy_commit:
                completeRegister();
                break;
        }
    }

    private void completeRegister() {
        if(TextUtils.isEmpty(register_healthy_blood.getText().toString().trim())){
            ToastUtils.makeText(mContext,"请输入血压");
            return;
        }
        if(TextUtils.isEmpty(register_healthy_weight.getText().toString().trim())){
            ToastUtils.makeText(mContext,"请输入体重");
            return;
        }
        if(TextUtils.isEmpty(register_healthy_height.getText().toString().trim())){
            ToastUtils.makeText(mContext,"请输入身高");
            return;
        }
        if(TextUtils.isEmpty(register_healthy_history.getText().toString().trim())){
            ToastUtils.makeText(mContext,"请输入病史");
            return;
        }

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initList() {

    }
}
