package com.example.admin.ccb.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.admin.ccb.R;
import com.example.admin.ccb.activity.CommodityDetailsActivity;
import com.example.admin.ccb.adapter.NewHomeAdapter;
import com.example.admin.ccb.base.BaseFragment;
import com.example.admin.ccb.bean.GoodsManage;
import com.example.admin.ccb.bean.GoodsManageList;
import com.example.admin.ccb.config.URLConfig;
import com.example.admin.ccb.utils.GlideImageLoader;
import com.example.admin.ccb.utils.ResCcb;
import com.gyf.barlibrary.ImmersionBar;
import com.lzy.okgo.model.HttpParams;
import com.yanzhenjie.recyclerview.swipe.SwipeItemClickListener;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuRecyclerView;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


public class NewHomeFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.frag_home_list)
    SwipeMenuRecyclerView recyclerView;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout refresh_layout;
    @BindView(R.id.frag_home_title)
    RelativeLayout frag_home_title;
    private Banner banner;
    private View headerView;
    private NewHomeAdapter adapter;
    private TextView home_jiankangzhigong, home_kuaijingjiankang, home_gongsizhigong, home_quanqiucaigou;
    private ImageView home_tuangoumiaosha, home_gexingdingzhi;
    private TabLayout home_tab;
    private int hotpage, tuipage;
    private int hotpageSize, tuipageSize;
    private int type = 1;
    private List<GoodsManage> goodsManages = new ArrayList<>();

    public static NewHomeFragment newInstance() {
        NewHomeFragment fragment = new NewHomeFragment();
        return fragment;
    }

    /**
     * 刷新。
     */
    private SwipeRefreshLayout.OnRefreshListener mRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            recyclerView.loadMoreFinish(false, true);
            initData();
            loadData();
        }

    };


    /**
     * 加载更多。
     */
    private SwipeMenuRecyclerView.LoadMoreListener mLoadMoreListener = new SwipeMenuRecyclerView.LoadMoreListener() {
        @Override
        public void onLoadMore() {
            // 数据完更多数据，一定要掉用这个方法。
            // 第一个参数：表示此次数据是否为空。
            // 第二个参数：表示是否还有更多数据。
//            recyclerView.loadMoreFinish(false, true);
            loadData();
            // 如果加载失败调用下面的方法，传入errorCode和errorMessage。
            // errorCode随便传，你自定义LoadMoreView时可以根据errorCode判断错误类型。
            // errorMessage是会显示到loadMoreView上的，用户可以看到。
            // mRecyclerView.loadMoreError(0, "请求网络失败");
        }

    };

    /**
     * Item点击监听。
     */
    private SwipeItemClickListener mItemClickListener = new SwipeItemClickListener() {
        @Override
        public void onItemClick(View itemView, int position) {
            Toast.makeText(getActivity(), "第" + position + "个", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected View initContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_home_new, container, false);
    }

    @Override
    public void initView(View view) {
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        adapter = new NewHomeAdapter(R.layout.item_home_good_info);
        initHeaderView();
        initData();
        banner = headerView.findViewById(R.id.hBanner);
        adapter.setHeaderView(headerView);
        recyclerView.setAdapter(adapter);
        refresh_layout.setOnRefreshListener(mRefreshListener); // 刷新监听。
        recyclerView.useDefaultLoadMore(); // 使用默认的加载更多的View。
        recyclerView.setLoadMoreListener(mLoadMoreListener); // 加载更多的监听。
        adapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {

            }
        });
    }

    private void initData() {
        hotpage = 1;
        tuipage = 1;
        tuipageSize = 10;
        hotpageSize = 10;
        goodsManages.clear();
    }

    private void initHeaderView() {

        headerView = View.inflate(mContext, R.layout.new_header_banner, null);
        home_jiankangzhigong = headerView.findViewById(R.id.home_jiankangzhigong);
        home_kuaijingjiankang = headerView.findViewById(R.id.home_kuaijingjiankang);
        home_gongsizhigong = headerView.findViewById(R.id.home_gongsizhigong);
        home_quanqiucaigou = headerView.findViewById(R.id.home_quanqiucaigou);
        home_tuangoumiaosha = headerView.findViewById(R.id.home_tuangoumiaosha);
        home_gexingdingzhi = headerView.findViewById(R.id.home_gexingdingzhi);
        home_tab = headerView.findViewById(R.id.home_tab);
        home_jiankangzhigong.setOnClickListener(this);
        home_kuaijingjiankang.setOnClickListener(this);
        home_gongsizhigong.setOnClickListener(this);
        home_quanqiucaigou.setOnClickListener(this);
        home_tuangoumiaosha.setOnClickListener(this);
        home_gexingdingzhi.setOnClickListener(this);
        home_tab.addTab(home_tab.newTab().setText("热销"));
        home_tab.addTab(home_tab.newTab().setText("推荐"));
        home_tab.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
//                recyclerView.loadMoreFinish(false, true);
                if (tab.getPosition() == 0) {
                    type=1;
                    initData();
                    getHot();

                } else {
                    type=2;
                    initData();
                    getTuijian();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

                Intent intent = new Intent(mContext, CommodityDetailsActivity.class);
                intent.putExtra("data",goodsManages.get(position));
                startActivity(intent);
            }
        });
    }

    @Override
    public void loadData() {
        if(type == 1)
        getHot();
        else
        getTuijian();
        getBanner();
    }

    private void getBanner() {
        HttpParams params = new HttpParams();
        okPostRequest("3",URLConfig.queryBannerList,params, com.example.admin.ccb.bean.Banner.class,"",false);
    }

    private void getTuijian() {
        HttpParams params = new HttpParams();
        params.put("pageNo", tuipage);
        params.put("pageSize", tuipageSize);
        params.put("marketingType", "02");
        okPostRequest("2", URLConfig.GoodsManageLis, params, GoodsManageList.class, "", false);
    }

    private void getHot() {
        HttpParams params = new HttpParams();
        params.put("pageNo", hotpage);
        params.put("pageSize", hotpageSize);
        params.put("marketingType", "01");
        okPostRequest("1", URLConfig.GoodsManageLis, params, GoodsManageList.class, "", false);
    }

    private GoodsManageList goodsManageList;

    @Override
    protected void okResponseSuccess(String whit, Object t) {
        super.okResponseSuccess(whit, t);
        if(whit.equals("1")){
            hotpage ++;
            refresh_layout.setRefreshing(false);
            Log.e("获取数据", t.toString());
            goodsManageList = (GoodsManageList) t;
//            Log.e("获取数据1233", ((GoodsManageList) t).getList().get(0).toString());
            goodsManages.addAll(goodsManageList.getList());
            adapter.setNewData(goodsManages);

            if(goodsManages.size() == ((GoodsManageList) t).getTotal()){
                recyclerView.loadMoreFinish(false, false);
            }else{
                recyclerView.loadMoreFinish(false, true);
            }
        }else if(whit.equals("2")){
            tuipage ++;
            refresh_layout.setRefreshing(false);
            goodsManageList = (GoodsManageList) t;
            goodsManages.addAll(goodsManageList.getList());
            adapter.setNewData(goodsManages);

            if(goodsManages.size() == ((GoodsManageList) t).getTotal()){
                recyclerView.loadMoreFinish(false, false);
            }else{
                recyclerView.loadMoreFinish(false, true);
            }
        }else if(whit.equals("3")){
            List<com.example.admin.ccb.bean.Banner> bannerList = (List<com.example.admin.ccb.bean.Banner>) t;
            setBannerOneDatas(bannerList);
        }


    }

    @Override
    protected void okResponseError(String whit, String body) {
        super.okResponseError(whit, body);
        refresh_layout.setRefreshing(false);
        recyclerView.loadMoreFinish(false, true);
    }

    private void setBannerOneDatas(List<com.example.admin.ccb.bean.Banner> advertList) {

        banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
        //设置图片加载器

        banner.setImageLoader(new GlideImageLoader());
        if (advertList == null || advertList.size() == 0) {
            //设置图片集合
            List<Integer> images = new ArrayList<>();
            images.add(R.mipmap.ic_launcher_round);
            banner.setImages(images);
        } else {
            //设置图片集合
            List<String> images = new ArrayList<>();
            for (int i = 0; i < advertList.size(); i++) {
                if(TextUtils.isEmpty(advertList.get(i).bannerPic))
                    images.add("http://img.xdbuty.com/rainbow/400927212376418c9596d5399fac98d4");
                else
                images.add(advertList.get(i).bannerPic);
            }
            banner.setImages(images);
            banner.setOnBannerListener(new OnBannerListener() {
                @Override
                public void OnBannerClick(int position) {

                }
            });
        }
        //设置banner动画效果
        banner.setBannerAnimation(Transformer.Default);
        //设置自动轮播，默认为true
        banner.isAutoPlay(true);
        //设置轮播时间
        banner.setDelayTime(5000);
        //设置指示器位置（当banner模式中有指示器时）
        banner.setIndicatorGravity(BannerConfig.RIGHT);
        //banner设置方法全部调用完毕时最后调用
        banner.start();
    }

    @Override
    public void initListener() {

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    protected void immersionInit() {
        mImmersionBar = ImmersionBar.with(this);  //可以为任意view;
        mImmersionBar.titleBarMarginTop(this.getView()).statusBarDarkFont(true, 0.2f).statusBarColor(R.color.color_theme) .init();
    }
}
