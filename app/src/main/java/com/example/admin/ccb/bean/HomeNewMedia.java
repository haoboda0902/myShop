package com.example.admin.ccb.bean;

import java.io.Serializable;
import java.util.List;

public class HomeNewMedia implements Serializable {

    private int total;
    private List<HomeNewMediaList> list;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<HomeNewMediaList> getList() {
        return list;
    }

    public void setList(List<HomeNewMediaList> list) {
        this.list = list;
    }
}
