package com.example.admin.ccb.activity;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.ccb.R;
import com.example.admin.ccb.base.BaseActivity;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuRecyclerView;

import butterknife.BindView;

/**
 * 健康直供
 *
 */

public class HealthySupply extends BaseActivity {

    @BindView(R.id.healthy_supply_back)
    ImageView healthy_supply_back;
    @BindView(R.id.healthy_supply_title)
    TextView healthy_supply_title;
    @BindView(R.id.healthy_supply_serch)
    ImageView healthy_supply_serch;
    @BindView(R.id.healthy_supply_type)
    ImageView healthy_supply_type;
    @BindView(R.id.healthy_supply_refresh)
    SwipeRefreshLayout healthy_supply_refresh;
    @BindView(R.id.healthy_supply_recyclerview)
    SwipeMenuRecyclerView healthy_supply_recyclerview;

    private String title;
    private int type;
    private int pageNo,pageSize;

    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            pageNo=1;
            pageSize =10;
            loadData();
        }
    };



    @Override
    public int getContentViewResource() {
        return R.layout.activity_healthy_supply;
    }

    @Override
    protected void initView() {
        getIntentData();
        UpTitle(title);
    }

    private void getIntentData() {
        Intent intent = getIntent();
        title = intent.getStringExtra("title");
        type = intent.getIntExtra("type",0);
    }

    @Override
    protected void initData() {

    }

    private void loadData() {
    }

    @Override
    protected void initList() {

    }
}
