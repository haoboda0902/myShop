package com.example.admin.ccb.utils;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

/**
 * 创建日期：2019/3/23 on 12:52
 */

public class SpanableUtils {
    /**
     * [ ]
     *
     * @param s
     * @param color
     * @return
     */
    public static SpannableString makeSpannableString(String s, int color) {
        ForegroundColorSpan span = new ForegroundColorSpan(color);
        return createSpan(s, span, s.indexOf("["), s.indexOf("]"));
    }

    public static SpannableString makeSpannableString(String t ,int start, int end, int color) {
        ForegroundColorSpan span = new ForegroundColorSpan(color);
        return createSpan(t, span, start,end);
    }

    public static SpannableString createSpan(CharSequence text, Object spann, int start, int end) {

        if (start < 0 || end <= 0 || spann == null) {
            return null;
        }

        SpannableString spannableString = new SpannableString(text);

        spannableString.setSpan(spann, start, end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        return spannableString;
    }
}
