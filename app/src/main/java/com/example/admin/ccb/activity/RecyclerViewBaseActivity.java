package com.example.admin.ccb.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.example.admin.ccb.R;
import com.example.admin.ccb.base.BaseActivity;
import com.example.admin.ccb.view.Header;

import butterknife.BindView;

/**
 * <br>描述：带有TabLayout的Viewpager的Activity
 */
public abstract class RecyclerViewBaseActivity extends BaseActivity {
    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.header)
    protected Header mHeader;
    protected CommonFragmentPagerAdapter adapter;

    @Override
    public int getContentViewResource() {
        return R.layout.recycler_base_activity;
    }

    @Override
    protected void initView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(getItemDecoration());
        setHeaderTitle(getTitleString());
        mRecyclerView.setAdapter(getAdapter());
        if (!TextUtils.isEmpty(getRightText())){
            mHeader.showRightSpace(true);
            mHeader.getRightTextView().setText(getRightText());
        }
    }

    public void setHeaderTitle(String s) {
        mHeader.getTitle().setText(s);
    }


    public abstract RecyclerView.Adapter getAdapter();

    public abstract RecyclerView.ItemDecoration getItemDecoration();

    public abstract String getTitleString();

    public String getRightText() {
        return null;
    }

}
