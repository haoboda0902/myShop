package com.example.admin.ccb.wxapi.utils;

import java.security.Key;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;

public class RSA {
  /**
   * 指定key的大小(支付宝的只需要改为2048即可)
   */
  private static int KEYSIZE = 1024;
  private static String RSA_ALGORITHM="RSA/ECB/PKCS1Padding";
  public static final String CHAR_ENCODING = "UTF-8";


  /**
   * 加密方法 source： 源数据
   */
  public static String encrypt(String source, String publicKey)
      throws Exception {
    Key key = getPublicKey(publicKey);
    /** 得到Cipher对象来实现对源数据的RSA加密 */
    Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
    cipher.init(Cipher.ENCRYPT_MODE, key);
    byte[] b = source.getBytes();
    /** 执行加密操作 */
    byte[] b1 = cipher.doFinal(b);
    return new String(Base64.encodeBase64(b1),CHAR_ENCODING);
  }

  /**
   * 解密算法 cryptograph:密文
   */
  public static String decrypt(String cryptograph, String privateKey)
      throws Exception {
    Key key = getPrivateKey(privateKey);
    /** 得到Cipher对象对已用公钥加密的数据进行RSA解密 */
    Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
    cipher.init(Cipher.DECRYPT_MODE, key);
    byte[] b1 = Base64.decodeBase64(cryptograph.getBytes());
    /** 执行解密操作 */
    byte[] b = cipher.doFinal(b1);
    return new String(b);
  }

  /**
   * 得到公钥
   *
   * @param key 密钥字符串（经过base64编码）
   */
  public static PublicKey getPublicKey(String key) throws Exception {
    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(
        Base64.decodeBase64(key.getBytes()));
    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    PublicKey publicKey = keyFactory.generatePublic(keySpec);
    return publicKey;
  }

  /**
   * 得到私钥
   *
   * @param key 密钥字符串（经过base64编码）
   */
  public static PrivateKey getPrivateKey(String key) throws Exception {
    PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(
        Base64.decodeBase64(key.getBytes()));
    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
    return privateKey;
  }

  public static String sign(String content, String privateKey) {
    String charset = CHAR_ENCODING;
    try {
      PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(
          Base64.decodeBase64(privateKey.getBytes()));
      KeyFactory keyf = KeyFactory.getInstance("RSA");
      PrivateKey priKey = keyf.generatePrivate(priPKCS8);

      Signature signature = Signature.getInstance("SHA1WithRSA");

      signature.initSign(priKey);
      signature.update(content.getBytes(charset));

      byte[] signed = signature.sign();

      return new String(Base64.encodeBase64(signed));
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "";
  }

  public static void main(String[] args) throws Exception {
    final String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC6+Z8Hr7qRSSzJD3qoFxnlYFYGnAxBxJymn2HTWEvwcWLuqcxDoKjjrVgHFUqVtRPq5SlzuxkDHq3hvvl+c2Ijf7MjElPjwhAZ6Y0iGArUblK3aW1PFKRjEYVWXC/VoIoynA4ll9RcDl81cdBp3SqXTgC/8GxyY+nXXjCe79hDaQIDAQAB";
    String privateKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALr5nwevupFJLMkPeqgXGeVgVgacDEHEnKafYdNYS/BxYu6pzEOgqOOtWAcVSpW1E+rlKXO7GQMereG++X5zYiN/syMSU+PCEBnpjSIYCtRuUrdpbU8UpGMRhVZcL9WgijKcDiWX1FwOXzVx0GndKpdOAL/wbHJj6ddeMJ7v2ENpAgMBAAECgYAPpUu1bPMDrUfeGzl8T1KPaxxHuU8dQcKJ/tRExyKzoAlTR0ocbpkzVBIFpbZ0CI7+HOb3FDTnme2a8kwWVLllciYJZ+jhpCQV4tc5/bksaNLAIjMdqAUgkU96zCjrhlcayav7zYuSqpN2cXA0ePd7aNkmFZjQy2Zv2SvgwONrcQJBAPgwTKDl7q34ln4B2yGcL0PTAitolkVGEYdZyvTGNR8irMtW77rViEMPvFxxAWWn/FB60FN1LxJ1GCF8a5Fymi8CQQDA3BzvLOeWKJjxj4h1cbhOM9pbLr3kVqKmvJ/15aCKPpPIjp9KodQRO6HvKDwohxG7ZX7jyTeLYG3X4pr3IU3nAkEA19gJjJTrstlS6Fts3Boc8Pt+E4ptxeleLTxmSeJnKKDbbiw5aV9zlf8Fbc/Jy0MBKGzm6O4s9fxn9Bdk5aWpeQJBALDf7mVocIileQdp1QOpJRgLtEIWDy/ASPHUYnv5eg/vrkesjei7nCul3jDhXLLqlVehkQtFunxIFtV8zRTDZZcCQDBzubFStbg+7uKnOOrre/WTmFeiK9814/UunADZvoStlaojuqPHUc5mOVC2a6bSZ70I2NRiITFdVzxtwyZGgOc=";

    String nihao = RSA.encrypt("848cf3e6d8714a3fb288d90b51290ae7",
        publicKey);
    System.out.println("加密:" + nihao);

    String miwen = "i3x/SwHxmMXFrEn/lkqfyTXfGpSvmBCyKwdQbgTXLsabEQKxjgsMOhs0amQpZbASF7DuLC8HzFEwn7Cx8f/7GdYeaTot3mQjf2mt8FDaLyxXx++WP2xbONW6Ei+ypxVWLa6TMPgzn5M87rMgf9phwhKxmuEn61Ar9tSCJG+y60w=";
    System.out.println("解密：" + RSA
        .decrypt(miwen, privateKey));
  }
}