package com.example.admin.ccb.activity;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.ccb.R;
import com.example.admin.ccb.adapter.LuckyAdapter;
import com.example.admin.ccb.view.LuckyHeader;

import java.util.Arrays;

import static android.view.View.OnClickListener;

/**
 * 创建日期：2019/3/23 on 17:10
 * 描述:幸运猜
 * 作者:wangfan
 */

public class LuckyActivity extends RecyclerViewBaseActivity {
    private LuckyAdapter luckyAdapter;
    private LuckyHeader mLuckyHeader;

    @Override
    protected void initView() {
        super.initView();
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mRecyclerView.getLayoutParams();
        layoutParams.leftMargin = getResources().getDimensionPixelOffset(R.dimen.dp10);
        layoutParams.rightMargin = getResources().getDimensionPixelOffset(R.dimen.dp10);
        layoutParams.topMargin = getResources().getDimensionPixelOffset(R.dimen.dp10);
        mRecyclerView.setLayoutParams(layoutParams);
        mRecyclerView.setBackground(getResources().getDrawable(R.drawable.shape_stroke_white_bottom));
    }

    @Override
    public RecyclerView.Adapter getAdapter() {
        luckyAdapter = new LuckyAdapter();
        mLuckyHeader = new LuckyHeader(this);
        luckyAdapter.addHeaderView(mLuckyHeader);
        return luckyAdapter;
    }

    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider));
        return dividerItemDecoration;
    }

    @Override
    public String getTitleString() {
        return "幸运猜";
    }

    @Override
    public String getRightText() {
        return "购买记录";
    }

    @Override
    protected void initData() {
        luckyAdapter.addData(Arrays.asList(new Object[]{
                new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object()
        }));
        mLuckyHeader.bindDatas(null);
    }

    @Override
    protected void initList() {
        mHeader.getRightTextView().setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                /**
                 * 购买记录
                 */
            }
        });
    }
}
