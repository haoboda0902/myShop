package com.example.admin.ccb.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.ccb.R;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.gyf.barlibrary.ImmersionBar;
import com.gyf.barlibrary.ImmersionFragment;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.HttpParams;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import www.ccb.com.common.utils.LogUtils;
import www.ccb.com.common.utils.ToastUtils;


public abstract class BaseFragment extends ImmersionFragment {

    private boolean isVisible;                  //是否可见状态
    private boolean isPrepared;                 //标志位，View已经初始化完成。
    protected LayoutInflater inflater;
    public ImmersionBar mImmersionBar;
    public Context mContext;
    private Gson gson;
    private Unbinder unbind;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.inflater = inflater;
        mContext = getActivity();
        gson = new Gson();
        View view = initContentView(inflater, container, savedInstanceState);
        unbind = ButterKnife.bind(this, view);
        initView(view);
        loadData();
        initListener();
        isPrepared = true;
        return view;
    }

    public void isTitleBar(boolean is, View v){
        if (is){
            mImmersionBar = ImmersionBar.with(this);  //可以为任意view;
            mImmersionBar.titleBarMarginTop(v).statusBarDarkFont(true, 0.2f).statusBarColor(R.color.white).init();
        }
    }

    /** 如果是与ViewPager一起使用，调用的是setUserVisibleHint */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisible = true;
            onVisible();
        } else {
            isVisible = false;
            onInvisible();
        }
    }

    /**
     * 如果是通过FragmentTransaction的show和hide的方法来控制显示，调用的是onHiddenChanged.
     * 若是初始就show的Fragment 为了触发该事件 需要先hide再show
     */
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            isVisible = true;
            onVisible();
        } else {
            isVisible = false;
            onInvisible();
        }

        if (!hidden && mImmersionBar != null)
            mImmersionBar.init();
    }

    protected void onVisible() {
    }
    protected void onInvisible() {
    }
    protected abstract View initContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);
    public abstract void initView(View view);
    public abstract void loadData();
    public abstract void initListener();

    public void okGetRequest(final String url){
        OkGo.<String>get(url).execute(new StringCallback() {
            @Override
            public void onStart(com.lzy.okgo.request.base.Request<String, ? extends com.lzy.okgo.request.base.Request> request) {
                super.onStart(request);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<String> response) {
                okResponseSuccess(url, response.body());
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<String> response) {
                super.onError(response);
                okResponseError(url, response.body());
            }

            @Override
            public void onFinish() {
                super.onFinish();
                okResponseFinish(url);
            }
        });
    }

    /**
     * OK网络请求 不需要传mobileLogin和JSESSIONID
     *
     * @param httpurl      请求URl 也用来标记
     * @param params       请求参数
     * @param clazz        返回的Bean对象
     * @param DialogMsg    弹出Dialog的文字消息
     * @param isShowDialog 是否弹出Dialog 默认弹出
     */
    public void okPostRequest(final String what, final String httpurl, HttpParams params, final Class clazz, final String DialogMsg, final boolean isShowDialog) {
        final String url = httpurl + "";
        params.put("mobileLogin", true);
        LogUtils.out(httpurl + what + "请求参数:__", params.toString());
        OkGo.<String>post(url).params(params).execute(new StringCallback() {
            @Override
            public void onStart(com.lzy.okgo.request.base.Request<String, ? extends com.lzy.okgo.request.base.Request> request) {
                super.onStart(request);
//                if (isShowDialog) showCbDialog(DialogMsg);
                okResponseStart( what);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<String> response) {
                LogUtils.out(httpurl + what + "请求结果:__", response.body());
                if (clazz == null) {
                    okResponseSuccess(what, response.body());
                } else {


                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if(!jsonObject.get("code").equals("success")){
                            String msg = jsonObject.getString("message");
                            okResponseError(what,msg);
                            return;
                        }
                        Object bean = gson.fromJson(jsonObject.get("data").toString(), clazz);

                        okResponseSuccess( what, bean);
                    } catch (Exception e) {
                        okResponseError(what,"网络出错");
                        ToastUtils.GsonExtremely();
                        LogUtils.e("异常信息：" + e.toString());
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<String> response) {
                super.onError(response);
                LogUtils.out( what + "请求结果:__", response.body());
                okResponseError( what, response.body());
                ToastUtils.failNetRequest();
            }

            @Override
            public void onFinish() {
                super.onFinish();
//                if (isShowDialog) dismissCbDialog();
                okResponseFinish( what);
            }
        });
    }
    /**
     * OK网络请求 不需要传mobileLogin和JSESSIONID
     *
     * 判断是否登录  不登录跳转登录
     *
     * @param httpurl      请求URl 也用来标记
     * @param params       请求参数
     * @param clazz        返回的Bean对象
     * @param DialogMsg    弹出Dialog的文字消息
     * @param isShowDialog 是否弹出Dialog 默认弹出
     */
    public void okPostRequestLogin(final String what, final String httpurl, HttpParams params, final Class clazz, final String DialogMsg, final boolean isShowDialog) {
        final String url = httpurl + "";
        params.put("mobileLogin", true);
        LogUtils.out(httpurl + what + "请求参数:__", params.toString());
        OkGo.<String>post(url).params(params).execute(new StringCallback() {
            @Override
            public void onStart(com.lzy.okgo.request.base.Request<String, ? extends com.lzy.okgo.request.base.Request> request) {
                super.onStart(request);
//                if (isShowDialog) showCbDialog(DialogMsg);
                okResponseStart( what);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<String> response) {
                LogUtils.out(httpurl + what + "请求结果:__", response.body());
                if (clazz == null) {
                    okResponseSuccess(what, response.body());
                } else {


                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if(!jsonObject.get("code").equals("success")){
                            String msg = jsonObject.getString("message");
                            okResponseError(what,msg);
                            return;
                        }
                        Object bean = gson.fromJson(jsonObject.get("data").toString(), clazz);

                        okResponseSuccess( what, bean);
                    } catch (Exception e) {
                        okResponseError(what,"网络出错");
                        ToastUtils.GsonExtremely();
                        LogUtils.e("异常信息：" + e.toString());
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<String> response) {
                super.onError(response);
                LogUtils.out( what + "请求结果:__", response.body());
                okResponseError( what, response.body());
                ToastUtils.failNetRequest();
            }

            @Override
            public void onFinish() {
                super.onFinish();
//                if (isShowDialog) dismissCbDialog();
                okResponseFinish( what);
            }
        });
    }

    /**
     * OK网络请求成功回调
     *
     * @param whit 请求标记
     * @param t    返回结果
     */
    protected void okResponseSuccess(String whit, Object t) {
    }

    /**
     * OK网络请求失败（错误）回调
     *
     * @param whit
     * @param body
     */
    protected void okResponseError(String whit, String body) {
    }

    /**
     * OK网络开始请求回调
     * @param flag
     */
    protected void okResponseStart(String flag) {
    }

    /**
     * OK网络请求完成回调
     * @param flag
     */
    protected void okResponseFinish(String flag) {
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        unbind.unbind();
        if (mImmersionBar != null)
            mImmersionBar.destroy();
    }
}