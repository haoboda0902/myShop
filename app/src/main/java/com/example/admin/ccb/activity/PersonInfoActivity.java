package com.example.admin.ccb.activity;

import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.admin.ccb.R;
import com.example.admin.ccb.base.BaseActivity;
import com.example.admin.ccb.view.MyOneLineView;

import butterknife.BindView;

public class PersonInfoActivity extends BaseActivity {

    @BindView(R.id.title)
    RelativeLayout title;
    @BindView(R.id.person_info_img)
    ImageView person_info_img;
    @BindView(R.id.person_info_nikename)
    MyOneLineView person_info_nikename;
    @BindView(R.id.person_main_realname)
    MyOneLineView person_main_realname;
    @BindView(R.id.person_main_card)
    MyOneLineView person_main_card;
    @BindView(R.id.person_main_phone)
    MyOneLineView person_main_phone;
    @BindView(R.id.persone_main_mail)
    MyOneLineView persone_main_mail;
    @BindView(R.id.person_main_pwd)
    MyOneLineView person_main_pwd;

    public static void create(Context context,String token){
        Intent intent = new Intent(context,PersonInfoActivity.class);
        intent.putExtra("token",token);
        context.startActivity(intent);
    }

    @Override
    public int getContentViewResource() {
        return R.layout.activity_person_info;
    }

    @Override
    protected void initView() {
        person_info_nikename.initMineNoicon(0,"用户名","",true);
        person_main_realname.initMineNoicon(0,"姓名","",true);
        person_main_card.initMineNoicon(0,"身份证号","",true);
        person_main_phone.initMineNoicon(0,"电话","",true);
        persone_main_mail.initMineNoicon(0,"邮箱","",true);
        person_main_pwd.initMineNoicon(0,"修改密码","",true);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initList() {

    }
}
