package com.example.admin.ccb.activity;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.example.admin.ccb.R;
import com.example.admin.ccb.base.BaseActivity;
import com.example.admin.ccb.view.Header;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.SlidingTabLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * <br>描述：带有TabLayout的Viewpager的Activity
 */
public abstract class TabLayoutViewpagerBaseActivity extends BaseActivity {
    @BindView(R.id.tab_layout)
    protected SlidingTabLayout mTab;
    @BindView(R.id.tab_viewPager)
    protected ViewPager mViewPager;
    @BindView(R.id.header)
    protected Header mHeader;
    protected CommonFragmentPagerAdapter adapter;

    @Override
    public int getContentViewResource() {
        return R.layout.tablayout_viewpager_base_activity;
    }

    @Override
    protected void initView() {
        adapter = new CommonFragmentPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(adapter);

        mTab.setViewPager(mViewPager, getTitles(), this, getFragments());

    }

    public void setHeaderTitle(String s) {
        mHeader.getTitle().setText(s);
    }

    public abstract String[] getTitles();
    public abstract ArrayList<Fragment> getFragments();

    public String getCurrentTitle() {
        return adapter.getTitleList().get(mViewPager.getCurrentItem());
    }
}
