package com.example.admin.ccb.bean;

import java.io.Serializable;
import java.util.List;

public class CommodityDetail implements Serializable {
    public List<BgGoodsSpecList> bgGoodsSpecList;
    public String buyType;
    public List<goodsBanner> goodsBannerList;
    public int goodsCategoryId;
    public String goodsMumber;
    public String goodsName;
    public String goodsRemark;
    public String marketingType;
    public String marketingTypeMc;
    public float retailPrice;
    public String smallPic;
    public String topCategoryName;
    public float vipPrice;
}
