package com.example.admin.ccb.activity;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.ccb.MainActivity;
import com.example.admin.ccb.R;
import com.example.admin.ccb.base.BaseActivity;
import com.example.admin.ccb.bean.LoginBean;
import com.example.admin.ccb.config.Config;
import com.example.admin.ccb.config.URLConfig;
import com.example.admin.ccb.utils.SPUtils;
import com.lzy.okgo.model.HttpParams;

import butterknife.BindView;
import butterknife.OnClick;
import www.ccb.com.common.utils.ToastUtils;

public class LoginActivity extends BaseActivity {


    @BindView(R.id.img_logo)
    ImageView img_logo;
    @BindView(R.id.login_et_name)
    EditText login_et_name;
    @BindView(R.id.login_et_pwd)
    EditText login_et_pwd;
    @BindView(R.id.login_btn_commit)
    Button login_btn_commit;
    @BindView(R.id.login_tv_forget_pwd)
    TextView login_tv_forget_pwd;
    @BindView(R.id.login_tv_register)
    TextView login_tv_register;


    public static Intent create(Context context){
        Intent intent = new Intent(context,LoginActivity.class);
        return intent;
    }

    @OnClick({R.id.login_btn_commit,R.id.login_tv_register})
    void login(View view){
        switch (view.getId()){
            case R.id.login_btn_commit:
                login();
                break;
            case R.id.login_tv_register:
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class).putExtra("token","klasjfkljsakljdfklsaj"));
                break;
        }

    }

    private void login() {
        if(TextUtils.isEmpty(login_et_name.getText().toString().trim())){
            ToastUtils.makeText(mContext,"请输入手机号");
            return;
        }
        if(TextUtils.isEmpty(login_et_pwd.getText().toString().trim())){
            ToastUtils.makeText(mContext,"请输入手机号");
            return;
        }
        HttpParams params = new HttpParams();
        params.put("mobile",login_et_name.getText().toString().trim());
        params.put("pwd",login_et_pwd.getText().toString());
        okPostRequest("1", URLConfig.loginIn,params, LoginBean.class,"登录中...",true);

    }

    @Override
    protected void okResponseSuccess(String whit, Object t) {
        super.okResponseSuccess(whit, t);
        LoginBean loginBean = (LoginBean) t;
        SPUtils.put(mContext, Config.USERIDKEY, loginBean.id);
        SPUtils.put(mContext,Config.TOKEN,loginBean.tokenId);
        this.finish();
    }

    @Override
    public int getContentViewResource() {
        return R.layout.activity_login;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initList() {

    }

}
