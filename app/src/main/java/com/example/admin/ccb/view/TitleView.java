package com.example.admin.ccb.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.admin.ccb.R;
import com.example.admin.ccb.base.AppController;
import com.tencent.mm.opensdk.utils.Log;


/**
 * @Description 通用标题栏
 * @Author JianZuming
 * @Date 2018/8/6
 * @Version V1.0.0
 * @Update 更新说明
 */
public class TitleView extends RelativeLayout {

    public static final int TYPE_HIDE = 0;
    public static final int TYPE_TEXT = 1;
    public static final int TYPE_IMG = 2;

    private boolean toggleBack;
    private String title;
    private int rightDrawableRes;
    private int leftDrawableRes;
    private int rightType;
    private String rightText;
    private int rightTextSize;
    private static final int RIGHT_TEXT_SIZE = 14;
    private int height;
    private ViewHolder holder;
    private int rightTextColor;

    public TitleView(Context context) {
        this(context, null);
    }

    public TitleView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);

    }

    public TitleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setBackgroundResource(android.R.color.white);
        if (isInEditMode()) {
            return;
        }
        // 获得属性
        init(context, attrs);

        // 设置布局
        addView();
        setView();

    }

    private void init(Context context, AttributeSet attrs) {
        height = context.getResources().getDimensionPixelOffset(R.dimen.dp_44);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TitleView);

        toggleBack = typedArray.getBoolean(R.styleable.TitleView_toggle_back, true);
        title = typedArray.getString(R.styleable.TitleView_title_text);
        // 默认右边不显示文字也不显示图片
        rightType = typedArray.getInt(R.styleable.TitleView_right_type, 0);
        rightText = typedArray.getString(R.styleable.TitleView_right_text);
        rightTextSize = typedArray.getDimensionPixelSize(R.styleable.TitleView_right_text_size, RIGHT_TEXT_SIZE);
        rightTextColor = typedArray.getColor(R.styleable.TitleView_right_text_color, 0);
        rightDrawableRes = typedArray.getResourceId(R.styleable.TitleView_right_drawable, 0);
        leftDrawableRes = typedArray.getResourceId(R.styleable.TitleView_left_drawable, 0);

        typedArray.recycle();
    }

    private void addView() {
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, height);
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_title_view, null);

        this.addView(view, layoutParams);

        holder = new ViewHolder(this);
    }

    private void setView() {
        if (!TextUtils.isEmpty(title)) {
            //throw new RuntimeException("the title can not be empty!");
            holder.mTitle.setText(title);
        }

        if (toggleBack) {
            holder.mBack.setVisibility(VISIBLE);
            holder.mBack.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onBackClickListener != null) {
                        onBackClickListener.onClick();
                        return;
                    }
                    if (getContext() instanceof Activity) ((Activity) getContext()).finish();
                    else AppController.getTopActivity().finish();
                }
            });
        } else {
            holder.mBack.setVisibility(GONE);
        }

        if (leftDrawableRes != 0) {
            holder.mLeftImg.setVisibility(VISIBLE);
            holder.mLeftImg.setImageResource(leftDrawableRes);

            holder.mLeftImg.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onLeftClickListener != null) {
                        onLeftClickListener.onClick(holder.mLeftImg);
                    }
                }
            });
        }

        switch (rightType) {
            case TYPE_TEXT:
                holder.mRightText.setVisibility(VISIBLE);
                if (!TextUtils.isEmpty(rightText))
                    holder.mRightText.setText(rightText);
                if (rightTextColor != 0)
                    holder.mRightText.setTextColor(rightTextColor);
                if (rightTextSize != RIGHT_TEXT_SIZE) {
                    holder.mRightText.setTextSize(TypedValue.COMPLEX_UNIT_PX, rightTextSize);
                }
                holder.mRightText.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onRightClickListener != null)
                            onRightClickListener.onClick(v);
                    }
                });

                break;
            case TYPE_IMG:
                holder.mRightImg.setVisibility(VISIBLE);
                if (rightDrawableRes != 0)
                    holder.mRightImg.setImageResource(rightDrawableRes);

                holder.mRightImg.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onRightClickListener != null)
                            onRightClickListener.onClick(v);
                    }
                });
                break;
            default:
                break;
        }
    }

    public void setTitleBg(@ColorRes int colorRes) {
        holder.mTitleBg.setBackgroundColor(getResources().getColor(colorRes));
    }

    public void setTitleDrawableBg(@DrawableRes int drawableRes) {
        holder.mTitleBg.setBackground(getResources().getDrawable(drawableRes));
    }

    public void setToggleBack(boolean toggleBack) {
        this.toggleBack = toggleBack;
        if (toggleBack) {
            holder.mBack.setVisibility(VISIBLE);
            if (getContext() instanceof Activity) ((Activity) getContext()).finish();
            else AppController.getTopActivity().finish();
        } else {
            holder.mBack.setVisibility(GONE);
        }
    }

    public void setTitle(String title) {
        holder.mTitle.setText(title);
    }

    public void setRightType(int rightType) {
        this.rightType = rightType;
        setView();
    }

    public void setRightText(String rightText) {
        if (rightType == 1)
            holder.mRightText.setText(rightText);
        else
            Log.e("","请定义title右侧类型为text");
    }

    public void setRightDrawableRes(int rightDrawableRes) {
        if (rightType != TYPE_IMG)
            Log.e("","请定义title右侧类型为img");
        this.rightDrawableRes = rightDrawableRes;
        holder.mRightImg.setImageResource(rightDrawableRes);

    }

    public void setRightTextGone() {
        holder.mRightText.setVisibility(GONE);
    }

    public void setRightTextVisible() {
        holder.mRightText.setVisibility(VISIBLE);
    }

    public void setRightEnable(boolean enable) {
        switch (rightType) {
            case 1:
                holder.mRightText.setEnabled(enable);
                holder.mRightText.setSelected(enable);
                break;
            case 2:
                holder.mRightImg.setEnabled(enable);
                holder.mRightImg.setSelected(enable);
                break;
            default:
                break;
        }
    }

    public interface OnRightClickListener {
        void onClick(View view);
    }

    public interface OnBackClickListener {
        void onClick();
    }

    private OnRightClickListener onRightClickListener;
    private OnRightClickListener onLeftClickListener;
    private OnBackClickListener onBackClickListener;

    public void setOnRightClickListener(OnRightClickListener onRightClickListener) {
        this.onRightClickListener = onRightClickListener;
    }

    public void setOnLeftClickListener(OnRightClickListener onRightClickListener) {
        this.onLeftClickListener = onRightClickListener;
    }

    public void setOnBackClickListener(OnBackClickListener onBackClickListener) {
        this.onBackClickListener = onBackClickListener;
    }

    private static class ViewHolder {
        private RelativeLayout mTitleBg;
        private TextView mTitle, mRightText;
        private ImageView mBack, mRightImg, mLeftImg;

        public ViewHolder(View view) {
            mTitleBg = view.findViewById(R.id.title_rl_bg);
            mTitle = (TextView) view.findViewById(R.id.title_view_title);
            mBack = (ImageView) view.findViewById(R.id.back);
            mRightText = (TextView) view.findViewById(R.id.right_text);
            mRightImg = (ImageView) view.findViewById(R.id.right_image);
            mLeftImg = (ImageView) view.findViewById(R.id.left_image);
        }
    }
}
