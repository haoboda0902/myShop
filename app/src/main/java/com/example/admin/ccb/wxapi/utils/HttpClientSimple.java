package com.example.admin.ccb.wxapi.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import www.ccb.com.common.utils.LogUtils;

public class HttpClientSimple {

  private static int bufferSize = 80920;
  private static int ConnectTimeout = 2 * 1000;
  private static int ReadTimeout = 10 * 1000;

  public static String doPost(String strURL, Map<String, Object> paramsMap,
      Map<String, String> headers) {
    BufferedReader reader = null;
    HttpURLConnection conn = null;
    try {
      StringBuffer param = new StringBuffer();
      if (paramsMap != null) {
        for (String key : paramsMap.keySet()) {
          param.append(key + "=" + paramsMap.get(key) + "&");
        }
      }
      param.deleteCharAt(param.length() - 1);

      URL url = new URL(strURL);
      conn = (HttpURLConnection) url.openConnection();
      conn.setDoInput(true);
      conn.setDoOutput(true);
      conn.setConnectTimeout(ConnectTimeout);// 2秒内未建立连接即断开
      conn.setReadTimeout(ReadTimeout);// 5秒内未获取返回即断开
      conn.setRequestMethod("POST");
      if (headers != null && headers.size() > 0) {
        for (String k : headers.keySet()) {
          conn.setRequestProperty(k, headers.get(k));
        }
      }

      DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
      dos.writeBytes(param.toString());
      dos.flush();
      dos.close();

      // Get the response
      reader = new BufferedReader(new InputStreamReader(
          conn.getInputStream()));
      char[] cbuf = new char[bufferSize];
      reader.read(cbuf);
      String result = new String(cbuf).trim();
      reader.close();
      int responseCode = conn.getResponseCode();
      System.out.println(
          "responseCode=" + responseCode + ",resultlength=(" + result.length() + "),result="
              + result);
      conn.disconnect();
      return result;
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (reader != null) {
          reader.close();
        }
        if (conn != null) {
          conn.disconnect();
        }
      } catch (Exception e2) {
        e2.printStackTrace();
      }
    }
    return "";
  }

  public static String doGet(String strURL, Map<String, String> headers) {
    BufferedReader reader = null;
    HttpURLConnection conn = null;
    try {
      URL url = new URL(strURL);
      conn = (HttpURLConnection) url.openConnection();
      conn.setDoOutput(true);
      conn.setConnectTimeout(ConnectTimeout);// 2秒内未建立连接即断开
      conn.setReadTimeout(ReadTimeout);// 5秒内未获取返回即断开
      if (headers != null && headers.size() > 0) {
        for (String k : headers.keySet()) {
          conn.setRequestProperty(k, headers.get(k));
        }
      }
      // Get the response
      reader = new BufferedReader(new InputStreamReader(
          conn.getInputStream()));
      char[] cbuf = new char[bufferSize];
      reader.read(cbuf);
      String result = new String(cbuf).trim();
      reader.close();
      int responseCode = conn.getResponseCode();
      System.out.println(
          "responseCode=" + responseCode + ",resultlength=(" + result.length() + "),result="
              + result);
      conn.disconnect();
      return result;
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (reader != null) {
          reader.close();
        }
        if (conn != null) {
          conn.disconnect();
        }
      } catch (Exception e2) {
        e2.printStackTrace();
      }
    }
    return "";
  }

  public static String doGet(String strURL, Map<String, Object> paramsMap,
      Map<String, String> headers) {
    StringBuilder urlsb = new StringBuilder(strURL);
    urlsb.append("?");
    if (paramsMap != null) {
      for (String key : paramsMap.keySet()) {
        urlsb.append(key + "=" + paramsMap.get(key) + "&");
      }
    }
    urlsb.deleteCharAt(urlsb.length() - 1);
    return doGet(urlsb.toString(), headers);
  }

  public static String doPostJson(String strURL, String postdata, Map<String, String> headers) {
    BufferedReader reader = null;
    DataOutputStream wr = null;
    HttpURLConnection conn = null;
    String applicationType="json";
    try {
      URL url = new URL(strURL);
      conn = (HttpURLConnection) url.openConnection();
      conn.setConnectTimeout(ConnectTimeout);// 2秒内未建立连接即断开
      conn.setReadTimeout(ReadTimeout);// 5秒内未获取返回即断开

      // 打开读写属性，默认均为false
      conn.setDoOutput(true);
      conn.setDoInput(true);

      // 设置请求方式，默认为GET
      conn.setRequestMethod("POST");

      // Post 请求不能使用缓存
      conn.setUseCaches(false);
      conn.setInstanceFollowRedirects(true);

      if (applicationType != null) {
        conn.setRequestProperty("Content-Type",
            "application/" + applicationType + ";charset=UTF-8");
        conn.setRequestProperty("Accept", "application/" + applicationType);
      }
      if (headers != null && headers.size() > 0) {
        for (String k : headers.keySet()) {
          conn.setRequestProperty(k, headers.get(k));
        }
      }

      wr = new DataOutputStream(conn.getOutputStream());
      wr.write(postdata.getBytes());
      wr.flush();
      // Get the response
      reader = new BufferedReader(new InputStreamReader(
          conn.getInputStream()));
      char[] cbuf = new char[bufferSize];
      reader.read(cbuf);
      String result = new String(cbuf).trim();
      reader.close();
      int responseCode = conn.getResponseCode();
      System.out.println(
          "responseCode=" + responseCode + ",resultlength=(" + result.length() + "),result="
              + result);
      conn.disconnect();
      return result;
    } catch (IOException e) {
      e.printStackTrace();
      LogUtils.e(e.toString());
    } finally {
      try {
        if (wr != null) {
          wr.close();
        }
        if (reader != null) {
          reader.close();
        }
        if (conn != null) {
          conn.disconnect();
        }
      } catch (Exception e2) {
        e2.printStackTrace();
      }
    }
    return null;
  }
}
