package com.example.admin.ccb.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.admin.ccb.R;


/**
 * <br>描述：通用的header
 */
public class Header extends RelativeLayout implements View.OnClickListener {

    private LinearLayout leftSpace, rightSpace;
    private ImageView btnLeft;
    private TextView tvRight, tvTitle;
    private OnBackListener onBackListener;

    public Header(Context context) {
        this(context, null, 0);
    }

    public Header(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Header(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(attrs);
    }

    private void init(AttributeSet attrs) {
        View view = inflate(getContext(), R.layout.widget_header, this);

        tvTitle =  findViewById(R.id.headerTitle);

        leftSpace =  findViewById(R.id.leftSpace);
        rightSpace = findViewById(R.id.rightSpace);

        btnLeft =  findViewById(R.id.headerBack);
        btnLeft.setOnClickListener(this);
        leftSpace.setOnClickListener(this);

        tvRight =  findViewById(R.id.headerMenu);

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.Header);
        String rightContent = typedArray.getString(R.styleable.Header_rightText);
        tvRight.setText(rightContent);

        int btnBg = typedArray.getResourceId(R.styleable.Header_btnBg, R.mipmap.nav_back_black);
        btnLeft.setImageDrawable(getResources().getDrawable(btnBg));

        String title = typedArray.getString(R.styleable.Header_title);
        tvTitle.setText(title);

        boolean leftSpaceDisplay = typedArray.getBoolean(R.styleable.Header_leftSpaceDisplay, true);
        showLeftSpace(leftSpaceDisplay);

        boolean rightSpaceDisplay = typedArray.getBoolean(R.styleable.Header_rightSpaceDisplay, false);
        showRightSpace(rightSpaceDisplay);

        int titleColor = typedArray.getColor(R.styleable.Header_headTextColor, Color.parseColor("#292929"));
        tvRight.setTextColor(titleColor);
        tvTitle.setTextColor(titleColor);

        typedArray.recycle();//回收资源
    }

    public void showLeftSpace(boolean show) {
        leftSpace.setVisibility(show ? VISIBLE : GONE);
    }

    public void showRightSpace(boolean show) {
        rightSpace.setVisibility(show ? VISIBLE : GONE);
    }

    public TextView getRightTextView() {
        return tvRight;
    }

    public ImageView getLeftButton() {
        return btnLeft;
    }

    public LinearLayout getLeftSpace() {
        return leftSpace;
    }

    public LinearLayout getRightSpace() {
        return rightSpace;
    }

    public TextView getTitle() {
        return tvTitle;
    }

    public void setTvTitleText(String stringText) {
        tvTitle.setText(stringText);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.headerBack:
            case R.id.leftSpace:
                if (getContext() instanceof Activity) {
                    ((Activity) getContext()).finish();
                    if (onBackListener != null) {
                        onBackListener.onBack();
                    }
                }
                break;
        }
    }

    public void setOnBackListener(OnBackListener onBackListener) {
        this.onBackListener = onBackListener;
    }

    public interface OnBackListener {
        void onBack();
    }
}
