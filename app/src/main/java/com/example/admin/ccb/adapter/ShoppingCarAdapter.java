package com.example.admin.ccb.adapter;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.admin.ccb.R;
import com.example.admin.ccb.bean.ShoppingCarBean;
import com.guanaj.easyswipemenulibrary.EasySwipeMenuLayout;

import java.util.List;

public class ShoppingCarAdapter extends BaseQuickAdapter<ShoppingCarBean,BaseViewHolder> {

    private ItemClickListener listener;

    public ShoppingCarAdapter(int layoutResId, @Nullable List<ShoppingCarBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(final BaseViewHolder holder, ShoppingCarBean item) {
        holder.setText(R.id.tv_goods_name,item.name);
        holder.setText(R.id.tv_num,item.count+"");
        holder.setText(R.id.tv_type_size, item.type+"");
        holder.setText(R.id.tv_goods_price, "￥ " + (Double.valueOf(item.price) ));
        if(item.id == 1){
            CheckBox checkBox =holder.getView(R.id.check_box);
            checkBox.setChecked(true);
        }else{
            CheckBox checkBox =holder.getView(R.id.check_box);
            checkBox.setChecked(false);
        }
        final EasySwipeMenuLayout easySwipeMenuLayout = holder.getView(R.id.action_bar);
        holder.setOnClickListener(R.id.check_box, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.ItemClickListener(v, holder.getPosition());
            }
        });
        //删除
        holder.setOnClickListener(R.id.right, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //侧滑初始化
                easySwipeMenuLayout.resetStatus();
                listener.ItemDeleteClickListener(v, holder.getPosition());
            }
        });
        //减
        holder.setOnClickListener(R.id.tv_reduce, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.ItemReduceClickListener(v, holder.getPosition());
            }
        });
        //加
        holder.setOnClickListener(R.id.tv_add, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.ItemAddClickListener(v, holder.getPosition());
            }
        });

    }
    public void setOnItemClickListener(ItemClickListener listener) {
        this.listener = listener;
    }

    public interface ItemClickListener {
        void ItemClickListener(View view, int position);

        void ItemDeleteClickListener(View view, int position);

        void ItemAddClickListener(View view, int position);

        void ItemReduceClickListener(View view, int position);
    }

}
