package com.example.admin.ccb.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.ccb.R;
import com.example.admin.ccb.base.BaseFragment;

import butterknife.BindView;

/**
 * 描述:积分
 */

public abstract class RecyclerFragment extends BaseFragment {
    @BindView(R.id.recyclerView2)
    protected RecyclerView mRecyclerView;

    @Override
    protected View initContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recycler_fragment, container,false);
    }

    @Override
    public void initView(View view) {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(getItemDecoration());
        mRecyclerView.setAdapter(getAdapter());
    }


    @Override
    protected void immersionInit() {

    }

    public abstract RecyclerView.Adapter getAdapter();

    public abstract RecyclerView.ItemDecoration getItemDecoration();
}
