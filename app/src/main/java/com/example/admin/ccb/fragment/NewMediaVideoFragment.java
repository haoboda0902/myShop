package com.example.admin.ccb.fragment;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.admin.ccb.R;
import com.example.admin.ccb.adapter.FragNewMediaVideoAdapter;
import com.example.admin.ccb.base.BaseFragment;
import com.example.admin.ccb.bean.HomeNewMediaVideoList;
import com.yanzhenjie.recyclerview.swipe.SwipeItemClickListener;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import www.ccb.com.common.utils.ToastUtils;

public class NewMediaVideoFragment extends BaseFragment {

    @BindView(R.id.frag_newMedia_video_refresh)
    SwipeRefreshLayout frag_newMedia_video_refresh;
    @BindView(R.id.frag_newMedia_video_list)
    SwipeMenuRecyclerView frag_newMedia_video_list;
    private int page, pageSize;
    private FragNewMediaVideoAdapter adapter;
    private List<HomeNewMediaVideoList> lists;

    /**
     * 刷新。
     */
    private SwipeRefreshLayout.OnRefreshListener mRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            frag_newMedia_video_list.loadMoreFinish(false, true);
            initData();
            loadData();
        }

    };


    /**
     * 加载更多。
     */
    private SwipeMenuRecyclerView.LoadMoreListener mLoadMoreListener = new SwipeMenuRecyclerView.LoadMoreListener() {
        @Override
        public void onLoadMore() {
            // 数据完更多数据，一定要掉用这个方法。
            // 第一个参数：表示此次数据是否为空。
            // 第二个参数：表示是否还有更多数据。
//            recyclerView.loadMoreFinish(false, true);
            loadData();
            // 如果加载失败调用下面的方法，传入errorCode和errorMessage。
            // errorCode随便传，你自定义LoadMoreView时可以根据errorCode判断错误类型。
            // errorMessage是会显示到loadMoreView上的，用户可以看到。
            // mRecyclerView.loadMoreError(0, "请求网络失败");
        }

    };
    private BaseQuickAdapter.OnItemClickListener onItemClickListener = new BaseQuickAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            ToastUtils.makeText(mContext,position+"");
        }
    };
    public static NewMediaVideoFragment newInstance() {
        NewMediaVideoFragment fragment = new NewMediaVideoFragment();
        return fragment;
    }

    @Override
    protected View initContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_newmedia_video, container, false);
    }

    @Override
    public void initView(View view) {
        initData();
        initRecyclerView();
    }

    private void initData() {
        page = 1;
        pageSize = 10;
        lists = new ArrayList<>();
        adapter = new FragNewMediaVideoAdapter(R.layout.item_frag_newmedia_video,lists);
    }

    private void initRecyclerView() {
        frag_newMedia_video_list.setLayoutManager(new GridLayoutManager(mContext,2));
        frag_newMedia_video_list.setAdapter(adapter);
        frag_newMedia_video_refresh.setOnRefreshListener(mRefreshListener);
        frag_newMedia_video_list.useDefaultLoadMore();
        frag_newMedia_video_list.setLoadMoreListener(mLoadMoreListener);
        adapter.setOnItemClickListener(onItemClickListener);
    }

    @Override
    public void loadData() {
        for(int i=0;i<20;i++){
            HomeNewMediaVideoList homeNewMediaVideoList = new HomeNewMediaVideoList();
            lists.add(homeNewMediaVideoList);
        }
        adapter.setNewData(lists);

    }

    @Override
    public void initListener() {

    }

    @Override
    protected void immersionInit() {

    }
}
