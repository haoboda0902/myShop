package com.example.admin.ccb.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.ccb.R;
import com.example.admin.ccb.base.BaseFragment;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class NewMediaFragment extends BaseFragment {

    @BindView(R.id.frag_newmedia_tab)
    TabLayout frag_newmedia_tab;
    @BindView(R.id.frag_newmedia_viewpager)
    ViewPager frag_newmedia_viewpager;

    private List<Fragment> fragments;

    private List<String> list = new ArrayList<>();

    public static NewMediaFragment newInstance() {
        NewMediaFragment fragment = new NewMediaFragment();
        return fragment;
    }


    @Override
    protected View initContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_newmedia, container, false);
    }

    @Override
    public void initView(View view) {
        isTitleBar(true, view);
        list.add("视频");
        list.add("直播");
        frag_newmedia_tab.addTab(frag_newmedia_tab.newTab().setText(list.get(0)));
        frag_newmedia_tab.addTab(frag_newmedia_tab.newTab().setText(list.get(1)));
        fragments = new ArrayList<>();
        NewMediaVideoFragment newMediaFragment = NewMediaVideoFragment.newInstance();
        NewMediaVideoFragment newMediaFragment2 = NewMediaVideoFragment.newInstance();
        fragments.add(newMediaFragment);
        fragments.add(newMediaFragment2);
        FragmentPagerAdapter mAdapter = new FragmentPagerAdapter(getFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public int getCount() {
                return fragments.size();
            }

            //ViewPager与TabLayout绑定后，这里获取到PageTitle就是Tab的Text
            @Override
            public CharSequence getPageTitle(int position) {
                return list.get(position);
            }
        };
        frag_newmedia_viewpager.setAdapter(mAdapter);

        frag_newmedia_tab.setupWithViewPager(frag_newmedia_viewpager);//将TabLayout和ViewPager关联起来。
        frag_newmedia_tab.setTabsFromPagerAdapter(mAdapter);//给Tabs设置适配器
    }


    @Override
    public void loadData() {

    }

    @Override
    public void initListener() {

    }

    @Override
    protected void immersionInit() {

    }
}
