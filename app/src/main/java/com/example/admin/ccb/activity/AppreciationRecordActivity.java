package com.example.admin.ccb.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;

import com.example.admin.ccb.R;
import com.example.admin.ccb.adapter.ApprecationRecordAdapter;
import com.example.admin.ccb.view.SpacesItemDecoration;

import java.util.Arrays;

/**
 * 创建日期：2019/3/23 on 10:09
 * 描述:升值记录
 */

public class AppreciationRecordActivity extends RecyclerViewBaseActivity {
    private ApprecationRecordAdapter mApprecationRecordAdapter;

    public static void open(Context context) {
        Intent intent = new Intent(context, AppreciationRecordActivity.class);
        context.startActivity(intent);
    }

    @Override
    public RecyclerView.Adapter getAdapter() {
        mApprecationRecordAdapter = new ApprecationRecordAdapter();
        return mApprecationRecordAdapter;
    }

    @Override
    public RecyclerView.ItemDecoration getItemDecoration() {
        return new SpacesItemDecoration((int) getResources().getDimension(R.dimen.dp10));
    }

    @Override
    public String getTitleString() {
        return "升值记录";
    }

    @Override
    protected void initData() {
        mApprecationRecordAdapter.addData(Arrays.asList(new Object[]{
                new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object()
        }));
    }

    @Override
    protected void initList() {

    }
}
