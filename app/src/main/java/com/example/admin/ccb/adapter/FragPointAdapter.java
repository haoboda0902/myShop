package com.example.admin.ccb.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.admin.ccb.bean.GoodsManage;
import com.example.admin.ccb.bean.HomePoint;
import com.example.admin.ccb.bean.HomePointList;

import java.util.List;

public class FragPointAdapter extends BaseQuickAdapter<HomePointList,BaseViewHolder> {

    public FragPointAdapter(int layoutResId, @Nullable List<HomePointList> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, HomePointList item) {

    }
}
