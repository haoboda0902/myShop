package com.example.admin.ccb.bean;

public class RegisterBean{


    /**
     * shareCode : B62LG7
     * saveCode : success
     * tokenId : j1AuVVbVhZ9J8kqkdG20190317233743
     * mobile : 18801134486
     * id : 11
     * state : 0
     * userName : 18801134486
     * cardNumber : VIP-2019031723375348C577TW
     */

    public String shareCode;
    public String saveCode;
    public String tokenId;
    public String mobile;
    public int id;
    public String state;
    public String userName;
    public String cardNumber;

    public String getShareCode() {
        return shareCode;
    }

    public void setShareCode(String shareCode) {
        this.shareCode = shareCode;
    }

    public String getSaveCode() {
        return saveCode;
    }

    public void setSaveCode(String saveCode) {
        this.saveCode = saveCode;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
