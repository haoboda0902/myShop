package com.example.admin.ccb.fragment;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.ccb.R;
import com.example.admin.ccb.adapter.FragPointAdapter;
import com.example.admin.ccb.base.BaseFragment;
import com.example.admin.ccb.bean.HomePoint;
import com.example.admin.ccb.bean.HomePointList;
import com.yanzhenjie.recyclerview.swipe.SwipeItemClickListener;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


public class PointFragment extends BaseFragment {

    @BindView(R.id.frag_point_refresh_layout)
    SwipeRefreshLayout frag_point_refresh_layout;
    @BindView(R.id.frag_point_list)
    SwipeMenuRecyclerView frag_point_list;
    private View headerView;
    private TextView frag_point_num,frag_point_account,frag_point_grade,frag_point_zengzhi;
    private ImageView frag_point_user_icon;
    private FragPointAdapter adapter;
    private int page,pageSize;
    private List<HomePointList> list;
    private HomePoint homePoint;
    /**
     * 刷新。
     */
    private SwipeRefreshLayout.OnRefreshListener mRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            frag_point_list.loadMoreFinish(false, true);
            initData();
            loadData();
        }

    };

    private void initData() {
            page =1;
            pageSize = 2;
        list = new ArrayList<>();
        adapter = new FragPointAdapter(R.layout.item_frag_point,list);
    }


    /**
     * 加载更多。
     */
    private SwipeMenuRecyclerView.LoadMoreListener mLoadMoreListener = new SwipeMenuRecyclerView.LoadMoreListener() {
        @Override
        public void onLoadMore() {
            loadData();
        }

    };

    /**
     * Item点击监听。
     */
    private SwipeItemClickListener mItemClickListener = new SwipeItemClickListener() {
        @Override
        public void onItemClick(View itemView, int position) {
            Toast.makeText(getActivity(), "第" + position + "个", Toast.LENGTH_SHORT).show();
        }
    };
    public static PointFragment newInstance() {
        PointFragment fragment = new PointFragment();
        return fragment;
    }


    @Override
    protected View initContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_point, container, false);
    }

    @Override
    public void initView(View view) {
        isTitleBar(true,view);
        initData();
        initHeaderView();
        initRecylerView();
        loadData();
    }

    private void initHeaderView() {
        headerView = View.inflate(getContext(),R.layout.layout_frag_point_header,null);
        frag_point_num = headerView.findViewById(R.id.frag_point_num);
        frag_point_account = headerView.findViewById(R.id.frag_point_account);
        frag_point_grade = headerView.findViewById(R.id.frag_point_grade);
        frag_point_zengzhi = headerView.findViewById(R.id.frag_point_zengzhi);
        frag_point_user_icon = headerView.findViewById(R.id.frag_point_user_icon);

    }
    private void initRecylerView() {
        frag_point_list.setLayoutManager(new LinearLayoutManager(mContext));
        adapter.setHeaderView(headerView);

        frag_point_list.setAdapter(adapter);
    }

    @Override
    public void loadData() {
        for(int i =0;i<5;i++){
            HomePointList homePointList = new HomePointList();
            list.add(homePointList);
        }
        adapter.setNewData(list);
    }

    @Override
    public void initListener() {

    }

    @Override
    protected void immersionInit() {

    }
}
