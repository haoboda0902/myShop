package com.example.admin.ccb.utils;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by jzm on 2018/4/24.
 * Description:
 */
public class AppUtils {

    private static Application sApplication;
    public static Context context;
    private static boolean isDebug;


    public static void init(Application app) {
        sApplication = app;
        context = app.getApplicationContext();
        isDebug = isAppDebug();
    }

    public static boolean isDebug() {
        return isDebug;
    }

    /**
     * 判断App是否是Debug版本，只在初始化执行一次
     *
     * @return {@code true}: 是{@code false}: 否
     */
    private static boolean isAppDebug() {
        if (context.getPackageName().isEmpty()) return false;
        try {
            PackageManager pm = context.getPackageManager();
            ApplicationInfo ai = pm.getApplicationInfo(context.getPackageName(), 0);
            return ai != null && (ai.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static Application getApp() {
        if (sApplication != null) return sApplication;
        try {
            @SuppressLint("PrivateApi")
            Class<?> activityThread = Class.forName("android.app.ActivityThread");
            Object at = activityThread.getMethod("currentActivityThread").invoke(null);
            Object app = activityThread.getMethod("getApplication").invoke(at);
            if (app == null) {
                throw new NullPointerException("u should init first");
            }
            init((Application) app);
            return sApplication;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        throw new NullPointerException("have not init");
    }
}

