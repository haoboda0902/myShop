package com.example.admin.ccb.utils;

import android.content.Context;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.widget.EditText;

import www.ccb.com.common.utils.ToastUtils;

/**
 * <br>包名：com.yingu.jr.utils
 * <br>项目名称：jr
 * <br>描述：输入检查
 * <br>创建人：BaoZhi
 * <br>创建时间：2016/7/1 0001 15:28
 */
public class InputVerifyUtil implements Commons.BASIC_INFO {

    private static final String VERIFY_CODE = "验证码";
    private static final String MOBILE = "手机号";
    private static final String LENTH_TIP_FORMAT = "%s必须为%d位";
    private static final String EMPTY_TIP_FORMAT = "请输入%s";
    private static final String LENTH_RANGE_FORMAT = "%s必须为%d~%d位字符";


    public static boolean checkVerifyCode(Context c, String code) {

        return checkLenth(c, code, VERIFY_CODE, VERIFY_CODE_LENTH);
    }

    public static boolean checkMobile(Context c, String mobile) {
        if (!checkLenth(c, mobile, MOBILE, MOBILE_LENTH)) {
            return false;
        }
        return true;
    }

    public static boolean checkMobile(String phone) {
        if (phone.length() < MOBILE_LENTH || phone.length() > MOBILE_LENTH) {
            return false;
        }
        return true;
    }



    public static boolean checkLenth(Context c, String input, String name, int minLen) {
        if (!checkEmpty(c, input, name)) {
            return false;
        }

        if (input.length() < minLen) {
            String strToast = String.format(LENTH_TIP_FORMAT, name, minLen);
            ToastUtils.showToast(c, strToast);
            return false;
        }

        return true;
    }



    public static boolean checkEmpty(Context c, String input, String name) {
        if (TextUtils.isEmpty(input)) {
            String strToast = String.format(EMPTY_TIP_FORMAT, name);
            ToastUtils.showToast(c, strToast);
            return false;
        }
        return true;
    }



    public static void setupVerifyCodeEt(EditText et) {
        et.setInputType(InputType.TYPE_CLASS_NUMBER);
        et.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
    }

    public static void setupMobileEt(EditText et) {
        et.setInputType(InputType.TYPE_CLASS_NUMBER);
        et.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
    }

}
