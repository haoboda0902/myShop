package com.example.admin.ccb.adapter;

import android.graphics.Paint;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.admin.ccb.R;
import com.example.admin.ccb.bean.GoodsManage;
import com.example.admin.ccb.utils.GlideImageUtils;

import java.text.DecimalFormat;

public class NewHomeAdapter extends BaseQuickAdapter<GoodsManage,BaseViewHolder> {


    public NewHomeAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, GoodsManage item) {
            helper.setText(R.id.home_good_name,item.goodsName)
            .setText(R.id.home_good_vip_price,"￥"+doublie(item.vipPrice))
            .setText(R.id.home_good_price,"￥"+doublie(item.retailPrice));
        ((TextView)helper.getView(R.id.home_good_price)).getPaint().setFlags(Paint. STRIKE_THRU_TEXT_FLAG );
        GlideImageUtils.Display(mContext,item.indexPic,(ImageView) helper.getView(R.id.home_good_img));
    }
    private String doublie(int pri){
        DecimalFormat df = new DecimalFormat("##0.00");

        return  df.format(pri);
    }
}
