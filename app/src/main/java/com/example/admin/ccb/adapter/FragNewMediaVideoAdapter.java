package com.example.admin.ccb.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.admin.ccb.bean.HomeNewMediaVideoList;

import java.util.List;

public class FragNewMediaVideoAdapter extends BaseQuickAdapter<HomeNewMediaVideoList,BaseViewHolder> {

    public FragNewMediaVideoAdapter(int layoutResId, @Nullable List<HomeNewMediaVideoList> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, HomeNewMediaVideoList item) {

    }
}
