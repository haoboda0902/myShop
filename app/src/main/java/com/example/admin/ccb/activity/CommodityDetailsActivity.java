package com.example.admin.ccb.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.LinearLayout;

import com.example.admin.ccb.R;
import com.example.admin.ccb.base.BaseActivity;
import com.example.admin.ccb.bean.GoodsManage;
import com.example.admin.ccb.fragment.CommodityDetailsFragment;
import com.example.admin.ccb.fragment.CommodityEvaluateFragment;
import com.example.admin.ccb.view.IndicatorLineUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class CommodityDetailsActivity extends BaseActivity {
    @BindView(R.id.commodity_details_tab)
    TabLayout commodity_details_tab;
    @BindView(R.id.commodity_details_viewpager)
    ViewPager commodity_details_viewpager;
    @BindView(R.id.commodity_details_title)
    LinearLayout commodity_details_title;
    private String[] titles = new String[]{"详情","评价"};

    private CommodityDetailsFragment commodityDetailsFragment;
    private CommodityEvaluateFragment commodityEvaluateFragment;
    private List<Fragment> fragments;
    private GoodsManage goodsManage;
    private double alpha=0;
    @Override
    public int getContentViewResource() {
        return R.layout.activity_commodity_details;
    }

    @Override
    protected void initView() {
        getIntentData();
        mImmersionBar.titleBarMarginTop(R.id.commodity_details_title).statusBarDarkFont(true, 0.2f).statusBarColor(R.color.white).init();
        fragments = new ArrayList<>();

        commodityDetailsFragment = CommodityDetailsFragment.newInstance(goodsManage);
        commodityEvaluateFragment = CommodityEvaluateFragment.newInstance();
        fragments.add(commodityDetailsFragment);
        fragments.add(commodityEvaluateFragment);
        FragmentAdapter adapter = new FragmentAdapter(getSupportFragmentManager());
        commodity_details_tab.setupWithViewPager(commodity_details_viewpager);
        commodity_details_tab.setTabTextColors(Color.argb((int) 0, 255,41,76),Color.argb((int) 0, 255,41,76));
        commodity_details_tab.setSelectedTabIndicatorColor(Color.argb(0, 235,97,0));
        commodity_details_tab.post(new Runnable() {
            @Override
            public void run() {
                IndicatorLineUtil.setIndicator(commodity_details_tab, 40, 40);
            }
        });
        commodity_details_viewpager.setAdapter(adapter);
        commodity_details_viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if(position == 1){
                    changeTitleBg2(255);
                }else{
                    changeTitleBg(alpha);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void getIntentData() {

        Intent intent = getIntent();
        goodsManage = (GoodsManage) intent.getSerializableExtra("data");

    }

    private class FragmentAdapter extends FragmentPagerAdapter{

        public FragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
    }
    @Override
    protected void initData() {

    }

    @Override
    protected void initList() {

    }
    public void changeTitleBg(double alpha){
        this.alpha = alpha;
        commodity_details_tab.setTabTextColors(Color.argb((int) alpha, 0,0,0),Color.argb((int) alpha, 235,97,0));
        commodity_details_title.setBackgroundColor(Color.argb((int) alpha, 255,255,255));
        commodity_details_tab.setSelectedTabIndicatorColor(Color.argb((int) alpha, 235,97,0));
    }
    public void changeTitleBg2(double alpha){
        commodity_details_tab.setTabTextColors(Color.argb((int) alpha, 0,0,0),Color.argb((int) alpha, 235,97,0));
        commodity_details_title.setBackgroundColor(Color.argb((int) alpha, 255,255,255));
        commodity_details_tab.setSelectedTabIndicatorColor(Color.argb((int) alpha, 235,97,0));
    }
}
