package com.example.admin.ccb.fragment;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.admin.ccb.R;
import com.example.admin.ccb.activity.LoginActivity;
import com.example.admin.ccb.activity.MyAddAddressActivity;
import com.example.admin.ccb.activity.PersonInfoActivity;
import com.example.admin.ccb.base.BaseFragment;
import com.example.admin.ccb.utils.PermissionHelper;
import com.gyf.barlibrary.ImmersionBar;

import butterknife.BindView;
import butterknife.OnClick;
import www.ccb.com.common.utils.ToastUtils;

public class MineFragment extends BaseFragment {

    @BindView(R.id.frag_mine_icon)
    ImageView frag_mine_icon;
    @BindView(R.id.frag_mine_setting)
    TextView frag_mine_setting;
    @BindView(R.id.frag_mine_bingouquan)
    TextView frag_mine_bingouquan;
    @BindView(R.id.frag_mine_jifen)
    TextView frag_mine_jifen;
    @BindView(R.id.frag_mine_name)
    TextView frag_mine_name;
    @BindView(R.id.frag_mine_renzheng)
    TextView frag_mine_renzheng;
    @BindView(R.id.frag_mine_vipnum)
    TextView frag_mine_vipnum;
    @BindView(R.id.frag_mine_invitation)
    TextView frag_mine_invitation;
    @BindView(R.id.frag_mine_order)
    TextView frag_mine_order;
    @BindView(R.id.frag_mine_collection)
    TextView frag_mine_collection;
    @BindView(R.id.frag_mine_address)
    TextView frag_mine_address;
    @BindView(R.id.frag_mine_healthy)
    TextView frag_mine_healthy;
    @BindView(R.id.frag_mine_noauthentication)
    LinearLayout frag_mine_noauthentication;
    @BindView(R.id.frag_mine_weijihuo)
    LinearLayout frag_mine_weijihuo;
    @BindView(R.id.frag_mine_viptype)
    TextView frag_mine_viptype;
    @BindView(R.id.frag_mine_vipdengji)
    TextView frag_mine_vipdengji;
    @BindView(R.id.frag_mine_jihuo)
    TextView frag_mine_jihuo;
    @BindView(R.id.frag_mine_yijihuo)
    LinearLayout frag_mine_yijihuo;
    @BindView(R.id.frag_mine_viptype2)
    TextView frag_mine_viptype2;
    @BindView(R.id.frag_mine_vipdengji2)
    TextView frag_mine_vipdengji2;
    @BindView(R.id.frag_mine_viptime)
    TextView frag_mine_viptime;
    @BindView(R.id.frag_mine_xufei)
    TextView frag_mine_xufei;

    public static MineFragment newInstance() {
        MineFragment fragment = new MineFragment();
        return fragment;
    }

    @Override
    protected View initContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_mine, container, false);
    }

    @Override
    public void initView(View view) {
        PermissionHelper.requestCamera(new PermissionHelper.OnPermissionGrantedListener() {
            @Override
            public void onPermissionGranted() {
                ToastUtils.makeText(mContext,"获取权限");
            }
        });
    }

    @OnClick({R.id.frag_mine_setting,R.id.frag_mine_invitation,R.id.frag_mine_order,R.id.frag_mine_collection,R.id.frag_mine_address,R.id.frag_mine_healthy,
    R.id.frag_mine_jihuo,R.id.frag_mine_xufei,R.id.frag_mine_icon})
    public void OnClick(View view){
        switch (view.getId()){
            case R.id.frag_mine_setting:
                Intent intent = new Intent(mContext, LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.frag_mine_invitation:
                break;
            case R.id.frag_mine_order:
                break;
            case R.id.frag_mine_collection:
                break;
            case R.id.frag_mine_address:
                getActivity().startActivity(MyAddAddressActivity.create(mContext));
                ToastUtils.makeText(mContext,"点击地址");
                break;
            case R.id.frag_mine_healthy:
                break;
            case R.id.frag_mine_jihuo:
                break;
            case R.id.frag_mine_xufei:
                break;
            case R.id.frag_mine_icon:
                PersonInfoActivity.create(mContext,"token");
                break;
        }
    }

    @Override
    public void loadData() {

    }

    @Override
    public void initListener() {

    }

    @Override
    protected void immersionInit() {
//        mImmersionBar = ImmersionBar.with(this);  //可以为任意view;
//        mImmersionBar.titleBarMarginTop(this.getView()).statusBarDarkFont(true, 0.2f).statusBarColor(R.color.color_theme) .init();
    }
}
