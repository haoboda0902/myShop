package com.example.admin.ccb.bean;

import java.io.Serializable;

public class HomeNewMediaList implements Serializable {

    private int id;
    private String mediaPic;
    private String mediaTitle;
    private String mediaType;
    private String mediaTypeMc;
    private String remark;
    private String videoLink;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMediaPic() {
        return mediaPic;
    }

    public void setMediaPic(String mediaPic) {
        this.mediaPic = mediaPic;
    }

    public String getMediaTitle() {
        return mediaTitle;
    }

    public void setMediaTitle(String mediaTitle) {
        this.mediaTitle = mediaTitle;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getMediaTypeMc() {
        return mediaTypeMc;
    }

    public void setMediaTypeMc(String mediaTypeMc) {
        this.mediaTypeMc = mediaTypeMc;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    @Override
    public String toString() {
        return "HomeNewMediaList{" +
                "id=" + id +
                ", mediaPic='" + mediaPic + '\'' +
                ", mediaTitle='" + mediaTitle + '\'' +
                ", mediaType='" + mediaType + '\'' +
                ", mediaTypeMc='" + mediaTypeMc + '\'' +
                ", remark='" + remark + '\'' +
                ", videoLink='" + videoLink + '\'' +
                '}';
    }
}
