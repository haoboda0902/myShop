package com.example.admin.ccb.wxapi.utils;

/**
 * @description: Digest用的转换工具类
 * @author: tangmin
 * @date: 2019-01-30 10:47:34
 * @version: 1.0
 */
public abstract class ConvertUtils {

  public static String toHex(byte input[]) {
    if (input == null) {
      return null;
    }
    StringBuffer output = new StringBuffer(input.length * 2);
    for (int i = 0; i < input.length; i++) {
      int current = input[i] & 0xff;
      if (current < 16) {
        output.append("0");
      }
      output.append(Integer.toString(current, 16));
    }

    return output.toString();
  }
}
