package com.example.admin.ccb.fragment;


import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.ccb.R;
import com.example.admin.ccb.adapter.FragShareAdapter;
import com.example.admin.ccb.base.BaseFragment;
import com.example.admin.ccb.bean.HomeShareList;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import www.ccb.com.common.utils.ToastUtils;

public class ShareFragment extends BaseFragment {

    @BindView(R.id.fab1)
    FloatingActionButton fab1;
    @BindView(R.id.fab2)
    FloatingActionButton fab2;
    @BindView(R.id.fab3)
    FloatingActionButton fab3;
    @BindView(R.id.menu_red)
    FloatingActionMenu menuRed;

    @BindView(R.id.frag_share_refresh_layout)
    SwipeRefreshLayout frag_share_refresh_layout;
    @BindView(R.id.frag_share_list)
    SwipeMenuRecyclerView frag_share_list;
    private View headerView;

    private int page,pageSize;
    private List<HomeShareList> lists;
    private FragShareAdapter adapter;
    public static ShareFragment newInstance() {
        ShareFragment fragment = new ShareFragment();
        return fragment;
    }




    @Override
    protected View initContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_share, container, false);
    }

    @Override
    public void initView(View view) {
        isTitleBar(true,view);
        initData();
        initAuctionView();
        initRecyclerView();
        initHeaderView();
    }

    private void initHeaderView() {

        headerView = View.inflate(mContext,R.layout.layout_frag_share_header,null);
        adapter.setHeaderView(headerView);
        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ToastUtils.makeText(mContext,"点击了HeaderView");
            }
        });

    }

    private void initData() {
        page = 1;
        pageSize=10;
        lists = new ArrayList<>();
        adapter = new FragShareAdapter(R.layout.item_frag_share,lists);
        frag_share_list.setAdapter(adapter);
    }

    private void initRecyclerView() {
        frag_share_list.setLayoutManager(new LinearLayoutManager(mContext));
        for(int i=0;i<10;i++){
            HomeShareList shareList = new HomeShareList();
            lists.add(shareList);
        }
        adapter.setNewData(lists);
    }

    private void initAuctionView() {
        menuRed.setClosedOnTouchOutside(true);
    }

    @OnClick({R.id.fab1,R.id.fab2, R.id.fab3})
    public void OnClick(View view){
        switch (view.getId()){
            case R.id.fab1:
                break;
            case R.id.fab2:
                break;
            case R.id.fab3:
                break;

        }
    }

    @Override
    public void loadData() {

    }

    @Override
    public void initListener() {

    }

    @Override
    protected void immersionInit() {

    }
}
