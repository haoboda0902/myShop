package com.example.admin.ccb.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.admin.ccb.R;
import com.example.admin.ccb.activity.CommodityDetailsActivity;
import com.example.admin.ccb.activity.ShoppingCartActivity;
import com.example.admin.ccb.adapter.FragCommodityDetailsPagerAdapter;
import com.example.admin.ccb.base.BaseFragment;
import com.example.admin.ccb.bean.CommodityDetail;
import com.example.admin.ccb.bean.GoodsManage;
import com.example.admin.ccb.bean.goodsBanner;
import com.example.admin.ccb.config.URLConfig;
import com.example.admin.ccb.view.NoScrollWebView;
import com.lzy.okgo.model.HttpParams;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import www.ccb.com.common.utils.UiUtils;

public class CommodityDetailsFragment extends BaseFragment {

    @BindView(R.id.frag_commodity_details_pager)
    ViewPager viewPager;
    @BindView(R.id.frag_commodity_details_pagernum)
    TextView pagernum;
    @BindView(R.id.frag_commodity_details_scroll)
    ScrollView scrollView;
    @BindView(R.id.frag_commodity_details_web)
    NoScrollWebView webView;
    @BindView(R.id.frag_commodity_details_name)
    TextView name;
    @BindView(R.id.frag_commodity_details_vipprice)
    TextView vipprice;
    @BindView(R.id.frag_commodity_details_realprice)
    TextView realprice;
    @BindView(R.id.frag_commodity_details_pai)
    TextView pai;
    @BindView(R.id.frag_commodity_details_address)
    TextView address;
    @BindView(R.id.frag_commodity_details_weight)
    TextView weight;
    @BindView(R.id.frag_commodity_details_collection)
    TextView collection;
    @BindView(R.id.frag_commodity_details_car)
    TextView car;
    @BindView(R.id.frag_commodity_details_buy)
    TextView buy;
    @BindView(R.id.frag_commodity_details_type)
    LinearLayout commodity_tpye;

    private FragCommodityDetailsPagerAdapter adapter;
    private GoodsManage goodsManage;
    private List<goodsBanner> goodsBannerList;
    private CommodityDetail commodityDetail;
    private String[] commodityType;
    public static CommodityDetailsFragment newInstance(GoodsManage goodsManage) {
        CommodityDetailsFragment fragment = new CommodityDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", goodsManage);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected View initContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_commodity_details, container, false);
    }

    private int height;  // 滑动到什么地方完全变色
    private int ScrollUnm = 0;  //滑动的距离总和

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void initView(View view) {
        initData();
        webView.loadUrl("https://blog.csdn.net/qq939782569/article/details/77941199");
        height = UiUtils.dp2px(mContext, 200 - 45);
        scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {
                if (i1 >= 255) {
                    ((CommodityDetailsActivity) getActivity()).changeTitleBg(255);
                } else if (i1 < 0) {
                    ((CommodityDetailsActivity) getActivity()).changeTitleBg(0);
                } else {
                    ((CommodityDetailsActivity) getActivity()).changeTitleBg(i1);
                }
            }
        });
        adapter = new FragCommodityDetailsPagerAdapter(goodsBannerList,mContext);
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pagernum.setText((position+1)+"/"+commodityDetail.goodsBannerList.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }



    private void initData() {
        Bundle bundle = this.getArguments();
        goodsManage = (GoodsManage) bundle.getSerializable("data");
        goodsBannerList = goodsManage.goodsBannerList;
    }

    @Override
    public void loadData() {
        HttpParams params = new HttpParams();
        params.put("id",goodsManage.goodsManageId);
        String url = URLConfig.queryGoodsdet;
        okPostRequest("1",url,params, CommodityDetail.class,"",false);
    }

    @Override
    protected void okResponseSuccess(String whit, Object t) {
        super.okResponseSuccess(whit, t);

        if(t!=null) {
            commodityDetail = (CommodityDetail) t;
            if(commodityDetail!=null)
            reView();
        }
    }

    private void reView() {
        name.setText(commodityDetail.goodsName);
        vipprice.setText(commodityDetail.vipPrice+"");
        realprice.setText(commodityDetail.retailPrice+"");
        commodityType = commodityDetail.marketingTypeMc.split(",");
        pagernum.setText(1+"/"+commodityDetail.goodsBannerList.size());
        initCommodifyType();
        car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ShoppingCartActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initCommodifyType() {
        if(commodityType==null || commodityType.length<1){
            commodity_tpye.setVisibility(View.GONE);
        }else{
            for(int i=0;i<commodityType.length;i++){
                View view = View.inflate(mContext,R.layout.layout_commodity_details_type,null);
                TextView textView = view.findViewById(R.id.frag_commodity_details_type_text);
                textView.setText(commodityType[i]);
                commodity_tpye.addView(view);
            }
        }

    }

    @Override
    public void initListener() {

    }

    @Override
    protected void immersionInit() {

    }

}
