package com.example.admin.ccb.utils;

/**
 * <br>包名：com.yingu.jr.common
 * <br>项目名称：jr
 * <br>描述：常用字段类
 * <br>创建人：BaoZhi
 * <br>创建时间：2016/5/9 0009 15:45
 */
public class Commons {

    public interface BASIC_INFO {

        int MIN_USER_NAME_LENTH = 6;
        int MAX_USER_NAME_LENTH = 20;

        int VERIFY_CODE_LENTH = 6;

        int MOBILE_LENTH = 11;

        int MAX_ID_CARD_LENTH = 18;

        /**
         * 港澳、台湾证件号码限制30位以内
         */
        int MAX_ID_CARD_LENTH_V1 = 30;


        /**
         * 手机号码
         * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188,1705
         * 联通：130,131,132,152,155,156,185,186,1709
         * 电信：133,1349,153,180,189,177,1700
         */
        //    NSString * MOBILE = @"^1((3//d|5[0-35-9]|8[025-9])//d|70[059])//d{7}$";//总况

        /**
         * 10         * 中国移动：China Mobile
         * 11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188，1705
         * 12
         */
        String CM = "^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d|705)\\d{7}$";
        /**
         * 15         * 中国联通：China Unicom
         * 16         * 130,131,132,152,155,156,185,186,1709
         * 17
         */
        String CU = "^1((3[0-2]|5[256]|8[56])\\d|709)\\d{7}$";
        /**
         * 20         * 中国电信：China Telecom
         * 21         * 133,1349,153,180,189,1700
         * 22
         */
        String CT = "^1((33|53|77|8[09])\\d|349|700)\\d{7}$";


        /**
         * 25         * 大陆地区固话及小灵通
         * 26         * 区号：010,020,021,022,023,024,025,027,028,029
         * 27         * 号码：七位或八位
         * 28
         */
        String PHS = "^0(10|2[0-5789]|\\d{3})\\d{7,8}$";


        String USERNAME_DIGITS = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890";
        String PWD_DIGITS = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890~!@#$%^&*-_.";
        String ID_CARD_DIGITS = "1234567890Xx";
    }

}
