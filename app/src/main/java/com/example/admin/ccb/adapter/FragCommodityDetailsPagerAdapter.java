package com.example.admin.ccb.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.admin.ccb.R;
import com.example.admin.ccb.bean.goodsBanner;
import com.example.admin.ccb.utils.GlideImageUtils;

import java.util.List;

public class FragCommodityDetailsPagerAdapter extends PagerAdapter {

    private List<goodsBanner> list;
    private Context context;

    public FragCommodityDetailsPagerAdapter(List<goodsBanner> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        View view = View.inflate(context, R.layout.item_frag_commodity_details_pager,null);
        ImageView imageView = view.findViewById(R.id.item_frag_commodity_details_pager_img);
        GlideImageUtils.Display(context,list.get(position).bgGoodsPic,imageView);
        container.addView(view);
        return view;
    }
}
