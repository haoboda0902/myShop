package com.example.admin.ccb.config;

public class URLConfig {

    /**
     * baseUrl
     */
    public static final String BASEURL = "http://120.77.241.172/bingo/api";
    /**
     * 首页商品列表
     */
    public static final String GoodsManageLis = BASEURL+"/goods/findBgIndexGoodsList.htm";
    /**
     * 商品详情
     */
    public static final String queryGoodsdet = BASEURL+"/goods/queryGoodsdet.htm";
    /**
     * Banner详情
     */
    public static final String queryBannerList = BASEURL+"/banner/queryBannerList.htm";
    /**
     * 获取验证码
     */
    public static final String getVerificationCode = BASEURL+"/login/getVerificationCode.htm";
    /**
     * 用户注册
     */
    public static final String userRegister = BASEURL+"/login/userRegister.htm";

    /**
     * 用户注册
     */
    public static final String loginIn = BASEURL+"/login/loginIn.htm.htm";



}
