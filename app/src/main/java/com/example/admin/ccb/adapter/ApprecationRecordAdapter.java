package com.example.admin.ccb.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.admin.ccb.R;
import com.example.admin.ccb.utils.SpanableUtils;

/**
 * 创建日期：2019/3/23 on 12:17
 * 描述:
 */

public class ApprecationRecordAdapter extends BaseQuickAdapter<Object, BaseViewHolder> {

    public ApprecationRecordAdapter() {
        super(R.layout.item_apprecation_record);
    }

    @Override
    protected void convert(BaseViewHolder helper, Object item) {
        helper.setText(R.id.date, "2018-8-18");
        helper.setText(R.id.info, "升值");
        helper.setText(R.id.number, "+88");
    }
}
