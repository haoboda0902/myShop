package com.example.admin.ccb.bean;

import java.io.Serializable;
import java.util.List;

public class GoodsManageList implements Serializable {
    private List<GoodsManage> list;
    private int total;
    private String code;
    private String message;
    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<GoodsManage> getList() {
        return list;
    }

    public void setList(List<GoodsManage> list) {
        this.list = list;
    }
}
