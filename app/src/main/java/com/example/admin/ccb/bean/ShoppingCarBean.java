package com.example.admin.ccb.bean;

import java.io.Serializable;

public class ShoppingCarBean implements Serializable {

    public String name;
    public int count;
    public int price;
    public int id;
    public int type;

}
