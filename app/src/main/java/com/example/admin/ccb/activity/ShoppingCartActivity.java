package com.example.admin.ccb.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.admin.ccb.R;
import com.example.admin.ccb.adapter.ShoppingCarAdapter;
import com.example.admin.ccb.base.BaseActivity;
import com.example.admin.ccb.bean.ShoppingCarBean;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuRecyclerView;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.OnClick;
public class ShoppingCartActivity extends BaseActivity implements ShoppingCarAdapter.ItemClickListener{

    @BindView(R.id.tvTitleBack)
    ImageView tvTitleBack;
    @BindView(R.id.shoppingcar_refresh_layout)
    SwipeRefreshLayout shoppingcar_refresh_layout;
    @BindView(R.id.shoppingcar__list)
    SwipeMenuRecyclerView shoppingcar__list;
    @BindView(R.id.all_chekbox)
    CheckBox all_chekbox;
    @BindView(R.id.tv_total_price)
    TextView tv_total_price;
    @BindView(R.id.tv_go_to_pay)
    TextView tv_go_to_pay;

    private double totalPrice = 0.00;
    private int totalCount = 0;
    private List<ShoppingCarBean> list;
    private ShoppingCarAdapter shoppingCarAdapter;

    @Override
    public int getContentViewResource() {
        return R.layout.activity_shoppingcar;
    }

    @Override
    protected void initView() {
        UpTitle("购物车");

        shoppingcar__list.setLayoutManager(new LinearLayoutManager(mContext));
        initDate();
        shoppingCarAdapter = new ShoppingCarAdapter(R.layout.item_shoppingcar,list);
        shoppingCarAdapter.setOnItemClickListener(this);
        shoppingcar__list.setAdapter(shoppingCarAdapter);
    }

    private void initDate() {
        list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            ShoppingCarBean shoppingCarBean = new ShoppingCarBean();
            shoppingCarBean.count =4;
            shoppingCarBean.id = 0;
            shoppingCarBean.price=5;
            shoppingCarBean.type=6;
            list.add(shoppingCarBean);
        }

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initList() {

    }

    @OnClick({R.id.all_chekbox, R.id.tv_go_to_pay})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.all_chekbox:
                AllTheSelected(true);
                break;
            case R.id.tv_go_to_pay:
                if (totalCount <= 0) {
                    Toast.makeText(this, "请选择要付款的商品~", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(this, "钱就是另一回事了~", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void ItemClickListener(View view, int position) {
        ShoppingCarBean shop = list.get(position);
        if (((CheckBox) view).isChecked()) {
            shop.id =1;
        } else {
            shop.id =0;
        }
        list.set(position, shop);
        AllTheSelected(false);
    }

    //删除
    @Override
    public void ItemDeleteClickListener(View view, int position) {
        list.remove(position);
        AllTheSelected(false);
    }

    //增加
    @Override
    public void ItemAddClickListener(View view, int position) {
        //当低于目标值是  可以做删除操作
        ShoppingCarBean shop = list.get(position);
        int currentCount = Integer.valueOf(shop.count);
        if (currentCount + 1 > 15) {
            Toast.makeText(this, "亲，已达库存上限~", Toast.LENGTH_SHORT).show();
        } else {
            shop.count +=1;
            list.set(position, shop);
        }
        AllTheSelected(false);
    }

    //减
    @Override
    public void ItemReduceClickListener(View view, int position) {
        ShoppingCarBean shop = list.get(position);
        int currentCount = Integer.valueOf(shop.count);
        if (currentCount - 1 < 1) {
            Toast.makeText(this, "受不了了，宝贝不能再减少了哦~", Toast.LENGTH_SHORT).show();
        } else {
            shop.count =currentCount-1;
            list.set(position, shop);
        }
        AllTheSelected(false);
    }

    //控制价格展示
    private void priceContro() {
        totalCount = 0;
        totalPrice = 0.00;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).id==1) {
                totalCount = totalCount + Integer.valueOf(list.get(i).count);
                double goodsPrice = Integer.valueOf(list.get(i).count) * Double.valueOf(list.get(i).price);
                totalPrice = totalPrice + goodsPrice;
            }
        }
        tv_total_price.setText("￥ " + totalPrice);
        tv_go_to_pay.setText("付款(" + totalCount + ")");
    }

    /**
     * 部分选取 做全选  全部选择做反选
     */
    private void AllTheSelected(Boolean aBoolean) {
        int number = 0;
        for (int j = 0; j < list.size(); j++) {
            if (list.get(j).id==1) {
                number++;
            }
        }
        if (aBoolean) {
            //全部选择  反选
            if (number == list.size()) {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).id=0;
                }
                all_chekbox.setChecked(false);
                //全部未选
            } else if (number == 0) {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).id=1;
                }
                all_chekbox.setChecked(true);
                //部分选择
            } else {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).id=1;
                }
                all_chekbox.setChecked(true);
            }
        } else {
            if (number == list.size()) {
                all_chekbox.setChecked(true);
            } else {
                all_chekbox.setChecked(false);
            }
        }

        shoppingCarAdapter.notifyDataSetChanged();
        priceContro();

    }

}
