package com.example.admin.ccb.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.ccb.activity.CommodityDetailsActivity;
import com.example.admin.ccb.base.BaseFragment;

public class CommodityEvaluateFragment extends BaseFragment {

    public static CommodityEvaluateFragment newInstance() {
        CommodityEvaluateFragment fragment = new CommodityEvaluateFragment();
        return fragment;
    }

    @Override
    protected View initContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return null;
    }

    @Override
    public void initView(View view) {

    }

    @Override
    public void loadData() {

    }

    @Override
    public void initListener() {

    }

    @Override
    protected void immersionInit() {

    }
}
