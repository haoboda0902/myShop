package com.example.admin.ccb.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.example.admin.ccb.R;
import com.example.admin.ccb.utils.SpanableUtils;

/**
 * 创建日期：2019/3/23 on 15:55
 * 描述:
 */

public class CouponHeader extends FrameLayout {
    private ImageView mHeaderImage;
    private TextView mName;
    private TextView mContent;
    private TextView mExpire;


    public CouponHeader(@NonNull Context context) {
        this(context, null);
    }

    public CouponHeader(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CouponHeader(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.coupon_header, this);
        mHeaderImage = findViewById(R.id.headerImage);
        mName = findViewById(R.id.name);
        mContent = findViewById(R.id.content);
        mExpire = findViewById(R.id.expire);
    }


    public void bindDatas(Object o) {
        loadRoundImage("http://www.pptbz.com/pptpic/UploadFiles_6909/201203/2012031220134655.jpg");
        mName.setText("好拨打");
        setContent(String.valueOf(12839));
        mExpire.setText(getResources().getString(R.string.expire,"2345"));
    }

    private void setContent(String t){
        String s = getResources().getString(R.string.coupon,t);
        mContent.setText(SpanableUtils.makeSpannableString(s,0,t.length(),getResources().getColor(R.color.color_fd4533)));
    }

    private void loadRoundImage(String url) {
        Glide.with(this).load(url).apply(RequestOptions.bitmapTransform(new CircleCrop())).into(mHeaderImage);
    }

}
