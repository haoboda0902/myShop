package com.example.admin.ccb.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.ccb.R;
import com.example.admin.ccb.base.BaseActivity;
import com.example.admin.ccb.bean.RegisterBean;
import com.example.admin.ccb.config.Config;
import com.example.admin.ccb.config.URLConfig;
import com.example.admin.ccb.utils.DialogUtils;
import com.example.admin.ccb.utils.SPUtils;
import com.example.admin.ccb.view.SendCodeTextView;
import com.lzy.okgo.model.HttpParams;
import com.uuzuche.lib_zxing.activity.CaptureActivity;
import com.uuzuche.lib_zxing.activity.CodeUtils;

import butterknife.BindView;
import butterknife.OnClick;
import www.ccb.com.common.utils.LogUtils;
import www.ccb.com.common.utils.ToastUtils;

public class RegisterActivity extends BaseActivity {
    @BindView(R.id.register_send_code)
    SendCodeTextView register_send_code;
    @BindView(R.id.tvTitleBack)
    ImageView tvTitleBack;
    @BindView(R.id.imgTitleRight)
    ImageView imgTitleRight;
    @BindView(R.id.register_recommender)
    EditText register_recommender;
    @BindView(R.id.register_name)
    EditText register_name;
    @BindView(R.id.register_mobile)
    EditText register_mobile;
    @BindView(R.id.register_verification_code)
    EditText register_verification_code;
    @BindView(R.id.register_pwd_pwd1)
    EditText register_pwd_pwd1;
    @BindView(R.id.register_pwd_pwd2)
    EditText register_pwd_pwd2;
    @BindView(R.id.register_default_recommender)
    TextView register_default_recommender;
    @BindView(R.id.register_no_recommender)
    TextView register_no_recommender;
    @BindView(R.id.register_goto_login)
    TextView register_goto_login;
    @BindView(R.id.register_commit)
    Button register_commit;
    private final int REQUEST_CODE = 106;
    @Override
    public int getContentViewResource() {
        return R.layout.activity_register;
    }

    @Override
    protected void initView() {
        mImmersionBar.titleBar(R.id.title).statusBarDarkFont(true, 0.2f).statusBarColor(R.color.color_themeE7).keyboardEnable(true).init();
    }

    @OnClick({R.id.tvTitleBack,R.id.imgTitleRight,R.id.register_default_recommender,R.id.register_no_recommender,
            R.id.register_goto_login,R.id.register_commit,R.id.register_send_code})
    public void OnClick(View view){
        switch (view.getId()){
            case R.id.tvTitleBack:
                RegisterActivity.this.finish();
                break;
            case R.id.imgTitleRight:
                Intent intent = new Intent(mContext, CaptureActivity.class);
                startActivityForResult(intent, REQUEST_CODE);
                break;
            case R.id.register_default_recommender:
                break;
            case R.id.register_no_recommender:
                break;
            case R.id.register_goto_login:
                Intent intentlogin = new Intent(mContext, LoginActivity.class);
                startActivity(intentlogin);
                this.finish();
                break;
            case R.id.register_commit:
                register();
                break;
            case R.id.register_send_code:
                getCode();
                break;
        }
    }

    private void register() {
//        startActivity(SetUserPwdActivity.create(mContext,"VIP-2019031822221437UB8VCE","SfNkBZ9jmAzbvdOLwN20190318222246"));
        if(TextUtils.isEmpty(register_mobile.getText().toString().trim())){
            ToastUtils.makeText(mContext,"请输入手机号");
            return;
        }
        if(TextUtils.isEmpty(register_verification_code.getText().toString().trim())){
            ToastUtils.makeText(mContext,"请输入验证码");
            return;
        }
        if(TextUtils.isEmpty(register_pwd_pwd1.getText().toString().trim())){
            ToastUtils.makeText(mContext,"请输入密码");
            return;
        }
        if(TextUtils.isEmpty(register_name.getText().toString().trim())){
            ToastUtils.makeText(mContext,"请输入用户名");
            return;
        }
        if(TextUtils.isEmpty(register_pwd_pwd2.getText().toString().trim())){
            ToastUtils.makeText(mContext,"请确认密码");
            return;
        }
        if(!register_pwd_pwd1.getText().toString().trim().equals(register_pwd_pwd2.getText().toString().trim())){
            ToastUtils.makeText(mContext,"两次输入的密码不一致");
            return;
        }
        HttpParams params = new HttpParams();
        params.put("mobile",register_mobile.getText().toString().trim());
        params.put("pwd",register_pwd_pwd1.getText().toString().trim());
        params.put("messageCode",register_verification_code.getText().toString().trim());
        if(!TextUtils.isEmpty(register_recommender.getText().toString().trim()))
        params.put("regCode",register_recommender.getText().toString().trim());
        okPostRequest("2", URLConfig.userRegister,params,RegisterBean.class,"注册中···",true);

    }

    //获取验证码
    private void getCode() {
        if(TextUtils.isEmpty(register_mobile.getText().toString().trim())){
            ToastUtils.makeText(mContext,"请输入验证码");
            return;
        }
        register_send_code.sendCountDownTimer();
        HttpParams params = new HttpParams();
        params.put("mobile",register_mobile.getText().toString().trim());
        params.put("reset",1);
        okPostRequest("1", URLConfig.getVerificationCode,params, null,"获取中···",true);

    }

    @Override
    protected void okResponseSuccess(String whit, Object t) {
        super.okResponseSuccess(whit, t);
        if(whit.equals("2")){
            RegisterBean registerBean = (RegisterBean) t;
            if(registerBean != null) {
                SPUtils.put(mContext, Config.USERIDKEY, registerBean.id);
                SPUtils.put(mContext,Config.TOKEN,registerBean.tokenId);
                startActivity(RegisterHealthyActivity.create(mContext,registerBean.cardNumber,registerBean.tokenId));
            }
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initList() {

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /**
         * 处理二维码扫描结果
         */
        if (requestCode == REQUEST_CODE) {
            //处理扫描结果（在界面上显示）
            if (null != data) {
                Bundle bundle = data.getExtras();
                if (bundle == null) {
                    return;
                }
                if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_SUCCESS) {
                    String result = bundle.getString(CodeUtils.RESULT_STRING);
                    DialogUtils.showDialogForIosStyle(mContext, "结果提示", result, "好", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            DialogUtils.dissmissDialog();
                        }
                    }, false);
                } else if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_FAILED) {
                    DialogUtils.showDialogForIosStyle(mContext, "提示", "扫码失败", "好", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            DialogUtils.dissmissDialog();
                        }
                    }, false);
                }
            }
        }
    }
}
