package com.example.admin.ccb.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.admin.ccb.R;
import com.example.admin.ccb.activity.AppreciationRecordActivity;
import com.example.admin.ccb.activity.TransferPointsActivity;
import com.example.admin.ccb.utils.SpanableUtils;

/**
 * 创建日期：2019/3/23 on 15:55
 * 描述:
 * 作者:wangfan
 */

public class IntegralHeader extends FrameLayout {
    private TextView mScore;
    private TextView mTransfer;
    private TextView mYesterday;
    private TextView mToday;
    private TextView mAppreciation;

    public IntegralHeader(@NonNull Context context) {
        this(context, null);
    }

    public IntegralHeader(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public IntegralHeader(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.intergra_header, this);
        mScore = findViewById(R.id.score);
        mTransfer = findViewById(R.id.transfer);
        mYesterday = findViewById(R.id.yesterday);
        mToday = findViewById(R.id.today);
        mAppreciation = findViewById(R.id.appreciation);
        mTransfer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                TransferPointsActivity.open(getContext());

            }
        });
        mAppreciation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AppreciationRecordActivity.open(getContext());
            }
        });
    }

    public void bindDatas(Object o) {
        mScore.setText("5999");
        mYesterday.setText(getResources().getString(R.string.yesterday, "5900"));
        String s = getResources().getString(R.string.today, "5900");
        mToday.setText(SpanableUtils.makeSpannableString(s, s.indexOf(":") + 1, s.length(), getResources().getColor(R.color.color_theme)));
    }

}
