package com.example.admin.ccb.activity;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.admin.ccb.R;
import com.example.admin.ccb.base.BaseActivity;
import com.example.admin.ccb.utils.InputVerifyUtil;
import com.example.admin.ccb.view.Header;
import com.example.admin.ccb.view.SendCodeTextView;

import butterknife.BindView;
import butterknife.OnClick;
import www.ccb.com.common.utils.ToastUtils;

/**
 * 创建日期：2019/3/23 on 9:56
 * 描述:积分转让
 */

public class TransferPointsActivity extends BaseActivity {
    @BindView(R.id.header)
    protected Header mHeader;
    @BindView(R.id.card_number)
    protected EditText mCardNumber;//会员卡号
    @BindView(R.id.userName)
    protected TextView mUserName;//用户名
    @BindView(R.id.full_name)
    protected EditText mFullName;//全名
    @BindView(R.id.phone)
    protected EditText mPhone;//手机号
    @BindView(R.id.getcode)
    protected SendCodeTextView mGetCode;//获取验证码
    @BindView(R.id.code)
    protected EditText mCode;//验证码

    public static void open(Context context) {
        Intent intent = new Intent(context, TransferPointsActivity.class);
        context.startActivity(intent);
    }

    @Override
    public int getContentViewResource() {
        return R.layout.activity_transfer_points;
    }

    @Override
    protected void initView() {
        mHeader.getRightSpace().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransferRecordActivity.open(TransferPointsActivity.this);
            }
        });
        InputVerifyUtil.setupMobileEt(mPhone);
        InputVerifyUtil.setupVerifyCodeEt(mCode);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initList() {

    }

    @OnClick({R.id.getcode, R.id.next})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.getcode:
                getCode();
                break;
            case R.id.next:
                break;
        }
    }

    //获取验证码
    private void getCode() {
        if (TextUtils.isEmpty(mCode.getText().toString().trim())) {
            ToastUtils.makeText(mContext, "请输入验证码");
            return;
        }
        mGetCode.sendCountDownTimer();

    }
}
