package com.example.admin.ccb.activity;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.admin.ccb.R;
import com.example.admin.ccb.base.BaseActivity;
import com.ywp.addresspickerlib.AddressPickerView;

import butterknife.BindView;
import butterknife.OnClick;
import www.ccb.com.common.utils.ToastUtils;

public class MyAddAddressActivity extends BaseActivity {

    @BindView(R.id.et_shouhuoren)
    EditText et_shouhuoren;
    @BindView(R.id.et_Mobile)
    EditText et_Mobile;
    @BindView(R.id.et_xiangxidizhi)
    EditText et_xiangxidizhi;
    @BindView(R.id.tv_city1)
    TextView tv_city1;
    @BindView(R.id.btn_save)
    Button btn_save;
    @BindView(R.id.tv_layout_city1)
    RelativeLayout tv_layout_city1;
    @BindView(R.id.apvAddress)
    AddressPickerView apvAddress;

    public static Intent create(Context context) {
        Intent intent = new Intent(context, MyAddAddressActivity.class);
        return intent;
    }

    @Override
    public int getContentViewResource() {
        return R.layout.activity_add_address;
    }

    @Override
    protected void initView() {
        UpTitle("添加收货地址");
        apvAddress.setOnAddressPickerSure(new AddressPickerView.OnAddressPickerSureListener() {
            @Override
            public void onSureClick(String address, String provinceCode, String cityCode, String districtCode) {
                ToastUtils.makeText(mContext,address);
            }
        });
    }

     @OnClick({R.id.btn_save,R.id.tv_layout_city1})
     public void OnClick(View view){
        switch (view.getId()){
            case R.id.btn_save:
                if(TextUtils.isEmpty(et_shouhuoren.getText().toString().trim())){
                    ToastUtils.makeText(mContext,"请输入姓名");
                    return;
                }
                if(TextUtils.isEmpty(et_Mobile.getText().toString().trim())){
                    ToastUtils.makeText(mContext,"请输入电话");
                    return;
                }
                if(TextUtils.isEmpty(et_xiangxidizhi.getText().toString().trim())){
                    ToastUtils.makeText(mContext,"请输入详细地址");
                    return;
                }
                if(TextUtils.isEmpty(tv_city1.getText().toString().trim())){
                    ToastUtils.makeText(mContext,"请选择地区");
                    return;
                }
                commit(et_shouhuoren.getText().toString().trim(),et_Mobile.getText().toString().trim(),et_xiangxidizhi.getText().toString().trim(),tv_city1.getText().toString().trim());
                break;
            case R.id.tv_layout_city1:
                apvAddress.setVisibility(View.VISIBLE);
                break;
        }
      
     }

    private void commit(String trim, String trim1, String trim2, String trim3) {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initList() {

    }
}
